## 部署方法
### 服务器
* fmmc 发送业务push
* fmmc-marketing 群发
* fmmc-marketing-2 群发

### 上传配置文件
如果修改了配置文件，需要执行以下命令重新上传：  
`cap production deploy:setup_config`  
如果要指定目标服务器：  
`HOSTS=fmmc-marketing,fmmc-marketing-2 cap production deploy:setup_config`

### 部署
`cap production deploy`

### 重启sidekiq
`cap production sidekiq:restart`  
可以登录web界面查看sidekiq运行情况

### 查看所有capistraino任务
`cap -T`

*注意：如果是测试环境，把production换成staging*


有问题请看wiki
