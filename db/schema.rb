# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141017082539) do

  create_table "auth_group", :force => true do |t|
    t.string "name", :limit => 80, :null => false
  end

  add_index "auth_group", ["name"], :name => "name", :unique => true

  create_table "auth_group_permissions", :force => true do |t|
    t.integer "group_id",      :null => false
    t.integer "permission_id", :null => false
  end

  add_index "auth_group_permissions", ["group_id", "permission_id"], :name => "group_id", :unique => true
  add_index "auth_group_permissions", ["group_id"], :name => "auth_group_permissions_5f412f9a"
  add_index "auth_group_permissions", ["permission_id"], :name => "auth_group_permissions_83d7f98b"

  create_table "auth_permission", :force => true do |t|
    t.string  "name",            :limit => 50,  :null => false
    t.integer "content_type_id",                :null => false
    t.string  "codename",        :limit => 100, :null => false
  end

  add_index "auth_permission", ["content_type_id", "codename"], :name => "content_type_id", :unique => true
  add_index "auth_permission", ["content_type_id"], :name => "auth_permission_37ef4eb4"

  create_table "auth_user", :force => true do |t|
    t.string   "password",     :limit => 128, :null => false
    t.datetime "last_login",                  :null => false
    t.boolean  "is_superuser",                :null => false
    t.string   "username",     :limit => 30,  :null => false
    t.string   "first_name",   :limit => 30,  :null => false
    t.string   "last_name",    :limit => 30,  :null => false
    t.string   "email",        :limit => 75,  :null => false
    t.boolean  "is_staff",                    :null => false
    t.boolean  "is_active",                   :null => false
    t.datetime "date_joined",                 :null => false
  end

  add_index "auth_user", ["username"], :name => "username", :unique => true

  create_table "auth_user_groups", :force => true do |t|
    t.integer "user_id",  :null => false
    t.integer "group_id", :null => false
  end

  add_index "auth_user_groups", ["group_id"], :name => "auth_user_groups_5f412f9a"
  add_index "auth_user_groups", ["user_id", "group_id"], :name => "user_id", :unique => true
  add_index "auth_user_groups", ["user_id"], :name => "auth_user_groups_6340c63c"

  create_table "auth_user_user_permissions", :force => true do |t|
    t.integer "user_id",       :null => false
    t.integer "permission_id", :null => false
  end

  add_index "auth_user_user_permissions", ["permission_id"], :name => "auth_user_user_permissions_83d7f98b"
  add_index "auth_user_user_permissions", ["user_id", "permission_id"], :name => "user_id", :unique => true
  add_index "auth_user_user_permissions", ["user_id"], :name => "auth_user_user_permissions_6340c63c"

  create_table "behavior_userbehavior", :force => true do |t|
    t.integer  "user_id",                  :null => false
    t.string   "api_path",  :limit => 200, :null => false
    t.string   "lang_code", :limit => 100, :null => false
    t.datetime "timestamp",                :null => false
  end

  create_table "celery_taskmeta", :force => true do |t|
    t.string   "task_id",                         :null => false
    t.string   "status",    :limit => 50,         :null => false
    t.text     "result",    :limit => 2147483647
    t.datetime "date_done",                       :null => false
    t.text     "traceback", :limit => 2147483647
    t.boolean  "hidden",                          :null => false
    t.text     "meta",      :limit => 2147483647
  end

  add_index "celery_taskmeta", ["hidden"], :name => "celery_taskmeta_2ff6b945"
  add_index "celery_taskmeta", ["task_id"], :name => "task_id", :unique => true

  create_table "celery_tasksetmeta", :force => true do |t|
    t.string   "taskset_id",                       :null => false
    t.text     "result",     :limit => 2147483647, :null => false
    t.datetime "date_done",                        :null => false
    t.boolean  "hidden",                           :null => false
  end

  add_index "celery_tasksetmeta", ["hidden"], :name => "celery_tasksetmeta_2ff6b945"
  add_index "celery_tasksetmeta", ["taskset_id"], :name => "taskset_id", :unique => true

  create_table "clockwork_database_events", :force => true do |t|
    t.integer  "frequency_quantity"
    t.integer  "frequency_period_id"
    t.string   "at"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "clockwork_database_events", ["frequency_period_id"], :name => "index_clockwork_database_events_on_frequency_period_id"

  create_table "commerce_deal", :force => true do |t|
    t.integer  "item_id",                                  :null => false
    t.integer  "buyer_id",                                 :null => false
    t.integer  "seller_id",                                :null => false
    t.decimal  "price",     :precision => 11, :scale => 2, :null => false
    t.integer  "state",                                    :null => false
    t.datetime "timestamp",                                :null => false
  end

  create_table "commerce_itemoffer", :force => true do |t|
    t.integer  "offer_thread_id",                                :null => false
    t.decimal  "price",           :precision => 11, :scale => 2, :null => false
    t.datetime "timestamp",                                      :null => false
    t.string   "comment"
    t.boolean  "from_buyer",                                     :null => false
  end

  add_index "commerce_itemoffer", ["offer_thread_id"], :name => "commerce_itemoffer_ce1ba42a"

  create_table "commerce_offerthread", :force => true do |t|
    t.integer "item_id",   :null => false
    t.integer "buyer_id",  :null => false
    t.boolean "deal_done", :null => false
    t.integer "seller_id", :null => false
  end

  create_table "commerce_publiccomment", :force => true do |t|
    t.integer  "item_id",     :null => false
    t.integer  "author",      :null => false
    t.string   "comment"
    t.datetime "timestamp",   :null => false
    t.integer  "ref_post_id", :null => false
  end

  create_table "commerce_rating", :force => true do |t|
    t.boolean  "for_seller", :null => false
    t.integer  "item_id",    :null => false
    t.integer  "rater_id",   :null => false
    t.integer  "ratee_id",   :null => false
    t.integer  "rate_num",   :null => false
    t.string   "comment"
    t.datetime "timestamp",  :null => false
  end

  create_table "dialcode_country", :force => true do |t|
    t.string  "countrycode",   :limit => 3,   :null => false
    t.string  "iso2",          :limit => 2,   :null => false
    t.integer "countryprefix",                :null => false
    t.string  "countryname",   :limit => 240, :null => false
  end

  create_table "dialcode_prefix", :primary_key => "prefix", :force => true do |t|
    t.string  "destination",  :limit => 180, :null => false
    t.integer "country_id"
    t.string  "carrier_name", :limit => 180, :null => false
    t.integer "prefix_type",                 :null => false
  end

  add_index "dialcode_prefix", ["country_id"], :name => "dialcode_prefix_a65e5e94"

  create_table "django_content_type", :force => true do |t|
    t.string "name",      :limit => 100, :null => false
    t.string "app_label", :limit => 100, :null => false
    t.string "model",     :limit => 100, :null => false
  end

  add_index "django_content_type", ["app_label", "model"], :name => "app_label", :unique => true

  create_table "django_session", :primary_key => "session_key", :force => true do |t|
    t.text     "session_data", :limit => 2147483647, :null => false
    t.datetime "expire_date",                        :null => false
  end

  add_index "django_session", ["expire_date"], :name => "django_session_b7b81f0c"

  create_table "django_site", :force => true do |t|
    t.string "domain", :limit => 100, :null => false
    t.string "name",   :limit => 50,  :null => false
  end

  create_table "djcelery_crontabschedule", :force => true do |t|
    t.string "minute",        :limit => 64, :null => false
    t.string "hour",          :limit => 64, :null => false
    t.string "day_of_week",   :limit => 64, :null => false
    t.string "day_of_month",  :limit => 64, :null => false
    t.string "month_of_year", :limit => 64, :null => false
  end

  create_table "djcelery_intervalschedule", :force => true do |t|
    t.integer "every",                :null => false
    t.string  "period", :limit => 24, :null => false
  end

  create_table "djcelery_periodictask", :force => true do |t|
    t.string   "name",            :limit => 200,        :null => false
    t.string   "task",            :limit => 200,        :null => false
    t.integer  "interval_id"
    t.integer  "crontab_id"
    t.text     "args",            :limit => 2147483647, :null => false
    t.text     "kwargs",          :limit => 2147483647, :null => false
    t.string   "queue",           :limit => 200
    t.string   "exchange",        :limit => 200
    t.string   "routing_key",     :limit => 200
    t.datetime "expires"
    t.boolean  "enabled",                               :null => false
    t.datetime "last_run_at"
    t.integer  "total_run_count",                       :null => false
    t.datetime "date_changed",                          :null => false
    t.text     "description",     :limit => 2147483647, :null => false
  end

  add_index "djcelery_periodictask", ["crontab_id"], :name => "djcelery_periodictask_7280124f"
  add_index "djcelery_periodictask", ["interval_id"], :name => "djcelery_periodictask_8905f60d"
  add_index "djcelery_periodictask", ["name"], :name => "name", :unique => true

  create_table "djcelery_periodictasks", :primary_key => "ident", :force => true do |t|
    t.datetime "last_update", :null => false
  end

  create_table "djcelery_taskstate", :force => true do |t|
    t.string   "state",     :limit => 64,         :null => false
    t.string   "task_id",   :limit => 36,         :null => false
    t.string   "name",      :limit => 200
    t.datetime "tstamp",                          :null => false
    t.text     "args",      :limit => 2147483647
    t.text     "kwargs",    :limit => 2147483647
    t.datetime "eta"
    t.datetime "expires"
    t.text     "result",    :limit => 2147483647
    t.text     "traceback", :limit => 2147483647
    t.float    "runtime"
    t.integer  "retries",                         :null => false
    t.integer  "worker_id"
    t.boolean  "hidden",                          :null => false
  end

  add_index "djcelery_taskstate", ["hidden"], :name => "djcelery_taskstate_2ff6b945"
  add_index "djcelery_taskstate", ["name"], :name => "djcelery_taskstate_4da47e07"
  add_index "djcelery_taskstate", ["state"], :name => "djcelery_taskstate_5654bf12"
  add_index "djcelery_taskstate", ["task_id"], :name => "task_id", :unique => true
  add_index "djcelery_taskstate", ["tstamp"], :name => "djcelery_taskstate_abaacd02"
  add_index "djcelery_taskstate", ["worker_id"], :name => "djcelery_taskstate_cac6a03d"

  create_table "djcelery_workerstate", :force => true do |t|
    t.string   "hostname",       :null => false
    t.datetime "last_heartbeat"
  end

  add_index "djcelery_workerstate", ["hostname"], :name => "hostname", :unique => true
  add_index "djcelery_workerstate", ["last_heartbeat"], :name => "djcelery_workerstate_11e400ef"

  create_table "entity_appsflyer", :force => true do |t|
    t.string   "app_id",           :limit => 50, :null => false
    t.string   "platform",         :limit => 50, :null => false
    t.string   "ip",               :limit => 20, :null => false
    t.string   "customer_user_id", :limit => 50, :null => false
    t.datetime "click_time"
    t.datetime "install_time"
    t.string   "agency",           :limit => 50, :null => false
    t.string   "media_source",     :limit => 50, :null => false
    t.string   "campaign",         :limit => 50, :null => false
    t.string   "campaign_id",      :limit => 50, :null => false
    t.string   "os_version",       :limit => 50, :null => false
    t.string   "sdk_version",      :limit => 50, :null => false
    t.string   "app_version",      :limit => 50, :null => false
    t.string   "click_url",                      :null => false
    t.string   "mac",              :limit => 50, :null => false
    t.string   "cost_per_install", :limit => 50, :null => false
    t.string   "country_code",     :limit => 50, :null => false
    t.string   "city",             :limit => 50, :null => false
    t.string   "language",         :limit => 50, :null => false
  end

  create_table "entity_category", :force => true do |t|
    t.integer  "parent_id",                   :null => false
    t.string   "title",        :limit => 50,  :null => false
    t.string   "slug",         :limit => 100, :null => false
    t.string   "description",  :limit => 200, :null => false
    t.integer  "status",       :limit => 2,   :null => false
    t.datetime "created_at",                  :null => false
    t.string   "short_url",    :limit => 200, :null => false
    t.datetime "publish_date"
    t.integer  "order_num",    :limit => 2,   :null => false
  end

  add_index "entity_category", ["parent_id"], :name => "entity_category_dda537fd"
  add_index "entity_category", ["status"], :name => "entity_category_48fb58bb"

  create_table "entity_facebookprofile", :force => true do |t|
    t.string   "first_name",           :limit => 50, :null => false
    t.string   "last_name",            :limit => 50, :null => false
    t.string   "name",                 :limit => 50, :null => false
    t.string   "email",                :limit => 50, :null => false
    t.string   "gender",               :limit => 20, :null => false
    t.boolean  "verified",                           :null => false
    t.string   "locale",               :limit => 10, :null => false
    t.integer  "timezone"
    t.string   "link",                               :null => false
    t.datetime "updated_time"
    t.integer  "age_range_min"
    t.integer  "age_range_max"
    t.string   "user_currency",        :limit => 20, :null => false
    t.float    "usd_exchange_inverse"
    t.float    "usd_exchange"
    t.integer  "currency_offset"
  end

  create_table "entity_image", :force => true do |t|
    t.integer "item_id",                     :null => false
    t.string  "image_path",   :limit => 200, :null => false
    t.integer "image_width"
    t.integer "image_height"
  end

  add_index "entity_image", ["item_id"], :name => "entity_image_40877d7e"

  create_table "entity_item", :force => true do |t|
    t.string   "fuzzy_item_id",   :limit => 50,                                         :null => false
    t.integer  "state",           :limit => 2,                                          :null => false
    t.string   "title",           :limit => 500,                                        :null => false
    t.text     "desc",            :limit => 2147483647,                                 :null => false
    t.decimal  "price",                                 :precision => 10, :scale => 2,  :null => false
    t.string   "currency",        :limit => 30,                                         :null => false
    t.string   "local_price",     :limit => 50,                                         :null => false
    t.decimal  "lat",                                   :precision => 13, :scale => 10, :null => false
    t.decimal  "lon",                                   :precision => 13, :scale => 10, :null => false
    t.integer  "user_id",                                                               :null => false
    t.datetime "created_at",                                                            :null => false
    t.string   "mediaLink",       :limit => 200,                                        :null => false
    t.integer  "internal_status", :limit => 2
    t.datetime "update_time"
    t.integer  "num_in_stock",                                                          :null => false
  end

  add_index "entity_item", ["fuzzy_item_id"], :name => "entity_item_8bc56882"
  add_index "entity_item", ["lat"], :name => "entity_item_c1108275"
  add_index "entity_item", ["lon"], :name => "entity_item_2aa42eca"
  add_index "entity_item", ["user_id"], :name => "entity_item_1ffdedc6"

  create_table "entity_itemcategory", :force => true do |t|
    t.integer "cat_id",  :null => false
    t.integer "item_id", :null => false
  end

  add_index "entity_itemcategory", ["cat_id"], :name => "entity_itemcategory_7efed46e"
  add_index "entity_itemcategory", ["item_id"], :name => "entity_itemcategory_40877d7e"

  create_table "entity_user", :force => true do |t|
    t.string   "fuzzy_user_id",          :limit => 50,                                         :null => false
    t.string   "email",                  :limit => 50,                                         :null => false
    t.string   "nickname",                                                                     :null => false
    t.string   "portrait",               :limit => 350,                                        :null => false
    t.datetime "created_at",                                                                   :null => false
    t.datetime "last_login_at"
    t.integer  "state",                  :limit => 2,                                          :null => false
    t.string   "first_name",             :limit => 50,                                         :null => false
    t.string   "last_name",              :limit => 50,                                         :null => false
    t.string   "password",               :limit => 100,                                        :null => false
    t.integer  "is_active",              :limit => 2,                                          :null => false
    t.integer  "account_type_id"
    t.integer  "upgrade_type_id"
    t.integer  "trade_status",           :limit => 2,                                          :null => false
    t.integer  "accumulate_credits"
    t.date     "start_date"
    t.date     "expire_date"
    t.text     "about",                  :limit => 2147483647,                                 :null => false
    t.string   "mobile_phone",           :limit => 30,                                         :null => false
    t.string   "mobile_phone_country",   :limit => 30,                                         :null => false
    t.string   "language",               :limit => 30,                                         :null => false
    t.string   "country",                :limit => 30,                                         :null => false
    t.integer  "seller_positive_rating",                                                       :null => false
    t.integer  "seller_total_rating",                                                          :null => false
    t.integer  "buyer_positive_rating",                                                        :null => false
    t.integer  "buyer_total_rating",                                                           :null => false
    t.decimal  "seller_rating_score",                          :precision => 7,  :scale => 5
    t.datetime "rating_update_time"
    t.decimal  "buyer_rating_score",                           :precision => 7,  :scale => 5
    t.decimal  "longitude",                                    :precision => 13, :scale => 10
    t.decimal  "latitude",                                     :precision => 13, :scale => 10
    t.string   "address",                :limit => 1000,                                       :null => false
    t.string   "from_channel",           :limit => 100,                                        :null => false
    t.string   "fb_user_id",             :limit => 50,                                         :null => false
    t.datetime "last_heartbeat_at"
    t.integer  "is_robot",               :limit => 2,                                          :null => false
    t.string   "appsflyer_user_id",      :limit => 50,                                         :null => false
    t.string   "city",                   :limit => 50,                                         :null => false
    t.string   "ip_address",             :limit => 20,                                         :null => false
  end

  add_index "entity_user", ["appsflyer_user_id"], :name => "entity_user_d1be994d"
  add_index "entity_user", ["email"], :name => "entity_user_830a6ccb"
  add_index "entity_user", ["from_channel"], :name => "entity_user_539827a6"
  add_index "entity_user", ["fuzzy_user_id"], :name => "entity_user_f8ef072a"
  add_index "entity_user", ["is_active"], :name => "entity_user_55f56498"
  add_index "entity_user", ["state"], :name => "entity_user_5654bf12"

  create_table "feedback_userfeedback", :force => true do |t|
    t.integer  "user_id",                          :null => false
    t.text     "text",       :limit => 2147483647, :null => false
    t.datetime "created_at",                       :null => false
  end

  create_table "fmmc_cron", :force => true do |t|
    t.string   "name",                :limit => 50,  :null => false
    t.boolean  "need_mail",                          :null => false
    t.boolean  "need_push",                          :null => false
    t.integer  "mail_template_id"
    t.integer  "push_template_id"
    t.integer  "status",              :limit => 2,   :null => false
    t.integer  "push_quota",                         :null => false
    t.string   "comment",                            :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "frequency_period_id"
    t.integer  "frequency_quantity",  :limit => 2,   :null => false
    t.string   "at",                  :limit => 100
    t.string   "tz",                  :limit => 100
  end

  add_index "fmmc_cron", ["frequency_period_id"], :name => "fmmc_cron_d6ee44e3"
  add_index "fmmc_cron", ["mail_template_id"], :name => "fmmc_cron_c0eb7c8f"
  add_index "fmmc_cron", ["push_template_id"], :name => "fmmc_cron_3e4e0f24"

  create_table "fmmc_frequency_period", :force => true do |t|
    t.string   "name",       :limit => 50, :null => false
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  create_table "fmmc_mail_template", :force => true do |t|
    t.string   "name",       :limit => 50,         :null => false
    t.text     "content",    :limit => 2147483647, :null => false
    t.string   "subject",    :limit => 100,        :null => false
    t.integer  "status",     :limit => 2,          :null => false
    t.string   "comment",                          :null => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "fmmc_push_template", :force => true do |t|
    t.string   "name",       :limit => 50,         :null => false
    t.text     "content",    :limit => 2147483647, :null => false
    t.integer  "status",     :limit => 2,          :null => false
    t.string   "comment",                          :null => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "payload",                          :null => false
  end

  create_table "south_migrationhistory", :force => true do |t|
    t.string   "app_name",  :null => false
    t.string   "migration", :null => false
    t.datetime "applied",   :null => false
  end

  create_table "spam_itemreport", :force => true do |t|
    t.integer  "item_id",      :null => false
    t.integer  "reporter_uid", :null => false
    t.integer  "reason",       :null => false
    t.datetime "timestamp",    :null => false
  end

  create_table "spam_itemreportrecord", :force => true do |t|
    t.integer  "reporter_id",                :null => false
    t.string   "item_id",      :limit => 20, :null => false
    t.integer  "reason",                     :null => false
    t.boolean  "is_processed",               :null => false
    t.datetime "updated_at"
    t.datetime "created_at"
  end

  create_table "spam_personreport", :force => true do |t|
    t.integer  "person_uid",   :null => false
    t.integer  "reporter_uid", :null => false
    t.integer  "reason",       :null => false
    t.datetime "timestamp",    :null => false
  end

  create_table "spam_userreportrecord", :force => true do |t|
    t.integer  "reporter_id",                :null => false
    t.string   "user_id",      :limit => 20, :null => false
    t.integer  "reason",                     :null => false
    t.boolean  "is_processed",               :null => false
    t.datetime "updated_at",                 :null => false
    t.datetime "created_at",                 :null => false
  end

  create_table "tastypie_apiaccess", :force => true do |t|
    t.string  "identifier",                   :null => false
    t.string  "url",                          :null => false
    t.string  "request_method", :limit => 10, :null => false
    t.integer "accessed",                     :null => false
  end

  create_table "tastypie_apikey", :force => true do |t|
    t.integer  "user_id",                :null => false
    t.string   "key",     :limit => 256, :null => false
    t.datetime "created",                :null => false
  end

  add_index "tastypie_apikey", ["user_id"], :name => "user_id", :unique => true

end
