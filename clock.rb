require_relative './config/boot'
require_relative './config/environment'
require 'clockwork'
require 'clockwork/database_events'
require 'message_worker'
require 'logger'

module Clockwork

  configure do |config|
    # config[:sleep_timeout] = 5
    # my_logger = Logger.new(Rails.root.join('log', 'clockwork.log'), 3, 15*1024*1024)
    # my_logger.formatter = Logger::Formatter.new
    # config[:logger] = my_logger
    # config[:tz] = 'EST'
    # config[:max_threads] = 15
    # config[:thread] = true
  end

  logger = Logger.new(STDOUT)

  # required to enable database syncing support
  Clockwork.manager = DatabaseEvents::Manager.new

  sync_database_events model: Cron, every: 1.minute do |cron|
    if cron.status == 0
      # Rails.logger.info "#{cron.name} was disabled, return"
      logger.info "#{cron.name} was disabled, return"
      next
    end

    # Rails.logger.info "#{cron.name} started"
    logger.info "#{cron.name} started"

    task_cls = Object.const_get(cron.task.clazz)
    task_cls.send :perform_async, cron.task.id
  end

  # on(:before_run) do |event, time|
  #   name = event.job
  #   # Job name is set to be "my_job::2014-08-04T18:00:00Z"
  #   if name =~  /^my_job::/
  #     start_at = Time.parse(name.split(/::/)[1])
  #     return start_at.day == time.day
  #   end
  #   true
  # end

  error_handler do |error|
    Raven.capture_exception(error)
  end
end
