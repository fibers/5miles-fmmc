require 'spec_helper'

describe Mobile::API do
  describe "GET /api/v1/user/:user_id/messages" do
    it "returns the message list for a user" do
      get "/api/v1/user/1/messages"
      response.status.should == 200
      JSON.parse(response.body).should == {"meta"=>{"limit"=>20, "offset"=>0, "total_count"=>1}, "objects"=>[{"message_type"=>"SYSTEM", "count"=>0, "last_message"=>"5 miles system messages"}]}
    end
  end
end