FactoryGirl.define do
=begin
  factory :user do
    name     "Michael Hartl"
    email    "michael@example.com"
    password "foobar"
    password_confirmation "foobar"
  end
=end

  factory :question do
    need_push true
    need_sms true
    buyer_id 1
    buyer_nick 'James'
    buyer_head '/image/avatar/james.png'
    seller_id 2
    seller_nick 'Henry'
    seller_head '/image/avatar/henry.png'
    content 'Great! I like this one, and how...'
    item_id 456
    item_image '/image/456.png'
    item_title 'some toy'
    is_reply false
    end

  factory :reply do
    need_push true
    need_sms true
    buyer_id 1
    buyer_nick 'James'
    buyer_head '/image/avatar/james.png'
    seller_id 2
    seller_nick 'Henry'
    seller_head '/image/avatar/henry.png'
    content 'blah blah blah...'
    item_id 456
    item_image '/image/456.png'
    item_title 'some toy'
    is_reply true
  end
end