require 'raven'

Raven.configure do |config|
  # 使用HTTPS会导致rake raven:test失败：Unable to record event with remote Sentry server (Faraday::TimeoutError - execution expired)
  config.dsn = Settings.raven_dsn 
  # config.dsn = 'http://d43b0a31abc34ef7baef9750dede8b14:ea8c57b7127f4c6dadb7a57d4117afef@app.getsentry.com/28078'
  config.environments = %w[ test production ]
end
