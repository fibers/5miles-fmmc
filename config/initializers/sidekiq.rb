require 'raven/sidekiq'

redis_conn = proc {
  Redis.new(:url => "redis://#{Settings.redis_host}:#{Settings.redis_port}/#{Settings.redis_db}", :namespace => Settings.redis_namespace)
}

Sidekiq.configure_server do |config|
  config.redis = { :url => "redis://#{Settings.redis_host}:#{Settings.redis_port}/#{Settings.redis_db}", :namespace => Settings.redis_namespace }
  # config.redis = ConnectionPool.new(size: 25, &redis_conn)
  config.error_handlers << Proc.new {|ex,ctx_hash| Raven.capture_exception(ex, ctx_hash) }
  config.server_middleware do |chain|
    chain.add Sidekiq::Throttler, storage: :redis
  end
end

Sidekiq.configure_client do |config|
  # config.redis = ConnectionPool.new(size: 25, &redis_conn)
  config.redis = { :url => "redis://#{Settings.redis_host}:#{Settings.redis_port}/#{Settings.redis_db}", :namespace => Settings.redis_namespace }
end

Sidekiq.default_worker_options = { 'backtrace' => true }

#Sidekiq.configure_client do |config|
#  config.redis = ConnectionPool.new(size: 5, &redis_conn)
#end
#Sidekiq.configure_server do |config|
#  config.redis = ConnectionPool.new(size: 25, &redis_conn)
#end