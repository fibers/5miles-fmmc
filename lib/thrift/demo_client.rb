require 'thrift'

require File.expand_path('../gen-rb/fmmc_api', __FILE__)
require File.expand_path('../gen-rb/fmmc_types', __FILE__)

begin
  port = ARGV[0] || 2048

  # transport = Thrift::BufferedTransport.new(Thrift::Socket.new('127.0.0.1', port))
  transport = Thrift::BufferedTransport.new(Thrift::Socket.new('54.79.0.193', port))
  protocol = Thrift::BinaryProtocol.new(transport)
  client = FmmcApi::Client.new(protocol)

  transport.open()

=begin
  begin
    question = QuestionDto.new
    question.item = Item.new
    question.item.id = 'item001'
    question.item.image = '/image/123.png'
    question.item.title = 'Glasses'
    question.buyer = Party.new
    question.buyer.id = 'dragon001'
    question.buyer.nick = 'James'
    question.buyer.head = '/image/avatar/james.png'
    question.seller = Party.new
    question.seller.id = 'dragon002'
    question.seller.nick = 'Henry'
    question.seller.head = '/image/avatar/henry.png'
    question.isReply = TRUE
    question.content = 'test'
    question.entryId = 22
    client.sendQuestion(question)
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end

  begin
    offer = OfferDto.new
    offer.item = Item.new
    offer.item.id = 'item001'
    offer.item.image = '/image/123.png'
    offer.item.title = 'Glasses'
    offer.buyer = Party.new
    offer.buyer.id = 'dragon001'
    offer.buyer.nick = 'James'
    offer.buyer.head = '/image/avatar/james.png'
    offer.seller = Party.new
    offer.seller.id = 'dragon002'
    offer.seller.nick = 'Henry'
    offer.seller.head = '/image/avatar/henry.png'
    offer.price = '$344.49'
    offer.comment = 'test'
    offer.sellSide = SellSide::BUYER_TO_SELLER
    offer.threadId = 444
    client.sendOffer(offer)
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end

  begin
    chat = ChatDto.new
    chat.item = Item.new
    chat.item.id = 'item001'
    chat.item.image = '/image/123.png'
    chat.item.title = 'Glasses'
    chat.buyer = Party.new
    chat.buyer.id = 'dragon001'
    chat.buyer.nick = 'James'
    chat.buyer.head = '/image/avatar/james.png'
    chat.seller = Party.new
    chat.seller.id = 'dragon002'
    chat.seller.nick = 'Henry'
    chat.seller.head = '/image/avatar/henry.png'
    chat.content = 'test'
    chat.threadId = 444
    chat.sellSide = SellSide::BUYER_TO_SELLER
    client.sendChat(chat)
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end

  begin
    feedback = FeedbackDto.new
    feedback.item = Item.new
    feedback.item.id = 'item001'
    feedback.item.image = '/image/123.png'
    feedback.item.title = 'Glasses'
    feedback.buyer = Party.new
    feedback.buyer.id = 'dragon001'
    feedback.buyer.nick = 'James'
    feedback.buyer.head = '/image/avatar/james.png'
    feedback.seller = Party.new
    feedback.seller.id = 'dragon002'
    feedback.seller.nick = 'Henry'
    feedback.seller.head = '/image/avatar/henry.png'
    feedback.rateNum = 4
    feedback.comment = 'test'
    feedback.sellSide = SellSide::BUYER_TO_SELLER
    feedback.threadId = 444
    client.sendFeedback(feedback)
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end

  begin
    message = MessageDto.new
    message.userId = 'dragon001'
    message.template = 'ITEM_CAT_CHANGED'
    message.placeholders = {  'item_title' => 'Glasses',
                              'cat_title' => 'xx'}
    client.sendMessage(message)
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end
=end

  begin
    # void push(1:string target_user, 2:string template, 3:string text, 4:map<string, string> data, 5:map<string, string> payload) throws (1:FmmcApiError error),
    client.push('2172', nil, 'test', nil, {'type' => 'system', 'action' => 'home'})
  rescue FmmcApiError => e
    print "FmmcApiError: ", e.errorMsg, "\n"
  end

  transport.close()

rescue Thrift::Exception => tx
  p tx
  print 'Thrift::Exception: ', tx.message, "\n"
end
