namespace cpp fmmc
namespace d fmmc
namespace java fmmc
namespace php fmmc
namespace perl fmmc

enum SellSide {
  BUYER_TO_SELLER = 0,
  SELLER_TO_BUYER = 1
}

struct Party {
    1: string id,
    2: string nick,
    3: string head,
}

struct Item {
    1: string id,
    2: string image,
    3: string title,
}

struct QuestionDto {
    1: Item item,
    2: Party buyer,
    3: Party seller,
    4: bool isReply,
    5: string content,
    6: i32 entryId,
}

struct OfferDto {
     1: Item item,
     2: Party buyer,
     3: Party seller,
     4: string price,
     5: string message,
     6: SellSide sellSide,
     7: i32 threadId,
     8: i32 entryId,
     9: bool accepted,
     10: i32 type,
}

struct FeedbackDto {
     1: Item item,
     2: Party buyer,
     3: Party seller,
     4: byte rateNum,
     5: string comment,
     6: SellSide sellSide,
     7: i32 threadId,
     8: i32 entryId,
}

struct MessageDto {
     1: string userId,
     2: string template,
     3: optional string content,
     4: optional map<string, string> placeholders,
}

exception FmmcApiError {
  1: i32 errorCode,
  2: string errorMsg,
}

service FmmcApi {
    void sendQuestion(1: QuestionDto questionDto) throws (1:FmmcApiError error),
    void sendOffer(1: OfferDto offerDto) throws (1:FmmcApiError error),
    void sendFeedback(1: FeedbackDto feedbackDto) throws (1:FmmcApiError error),
    void sendMessage(1: MessageDto messageDto) throws (1:FmmcApiError error),

    void send_mail(1:string target_user, 2:string template, 3:string text, 4:map<string, string> data, 5:string subject) throws (1:FmmcApiError error),
    void push(1:string target_user, 2:string template, 3:string text, 4:map<string, string> data, 5:map<string, string> payload) throws (1:FmmcApiError error),
    void notify(1:string target_user, 2:string cron_name, 3:map<string, string> data) throws (1:FmmcApiError error)
}