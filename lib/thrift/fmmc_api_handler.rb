require 'thrift'
require 'message_worker'

class FmmcApiHandler
  def initialize()
  end

  def sendQuestion(question_dto)
    begin
      if question_dto.isReply
        buyer_status = Constants::Status::UNREAD
        seller_status = Constants::Status::READ
      else
        buyer_status = Constants::Status::READ
        seller_status = Constants::Status::UNREAD
      end
      question = Question.create!(
          item_id: question_dto.item.id,
          item_image: question_dto.item.image,
          item_title: question_dto.item.title,
          buyer_id: question_dto.buyer.id,
          buyer_nick: question_dto.buyer.nick,
          buyer_head: question_dto.buyer.head,
          seller_id: question_dto.seller.id,
          seller_nick: question_dto.seller.nick,
          seller_head: question_dto.seller.head,
          is_reply: question_dto.isReply,
          content: question_dto.content,
          entry_id: question_dto.entryId,
          buyer_status: buyer_status,
          seller_status: seller_status
      )
      Rails.logger.info("qustion_id: #{question.id}, isReply: #{question_dto.isReply}, buyer_status: #{buyer_status}, seller_status: #{seller_status}")
      stage_push(question)

      MessageWorker::QuestionPushWorker.perform_async(question.id)
    rescue => e
      p e
      x = FmmcApiError.new
      x.errorCode = 9101
      x.errorMsg = 'send question error'
      raise x
    end
  end

  def sendOffer(offer_dto)
    begin
      if offer_dto.sellSide == Constants::SellSide::SELLER_TO_BUYER
        buyer_status = Constants::Status::UNREAD
        seller_status = Constants::Status::READ
      else
        buyer_status = Constants::Status::READ
        seller_status = Constants::Status::UNREAD
      end
      offer = Offer.create!(
          item_id: offer_dto.item.id,
          item_image: offer_dto.item.image,
          item_title: offer_dto.item.title,
          buyer_id: offer_dto.buyer.id,
          buyer_nick: offer_dto.buyer.nick,
          buyer_head: offer_dto.buyer.head,
          seller_id: offer_dto.seller.id,
          seller_nick: offer_dto.seller.nick,
          seller_head: offer_dto.seller.head,
          price: offer_dto.price,
          message: offer_dto.message,
          sell_side: offer_dto.sellSide,
          thread_id: offer_dto.threadId,
          entry_id: offer_dto.entryId,
          buyer_status: buyer_status,
          seller_status: seller_status,
          accepted: offer_dto.accepted
      )
      Rails.logger.info("offer_id: #{offer.id}, sell_side: #{offer_dto.sellSide}, buyer_status: #{buyer_status}, seller_status: #{seller_status}")
      stage_push(offer)

      MessageWorker::OfferPushWorker.perform_async(offer.id)
    rescue => e
      p e
      x = FmmcApiError.new
      x.errorCode = 9102
      x.errorMsg = 'send offer error'
      raise x
    end
  end

  def sendFeedback(feedback_dto)
    begin
      if feedback_dto.sellSide == Constants::SellSide::SELLER_TO_BUYER
        buyer_status = Constants::Status::UNREAD
        seller_status = Constants::Status::READ
      else
        buyer_status = Constants::Status::READ
        seller_status = Constants::Status::UNREAD
      end
      feedback = Feedback.create!(
          item_id: feedback_dto.item.id,
          item_image: feedback_dto.item.image,
          item_title: feedback_dto.item.title,
          buyer_id: feedback_dto.buyer.id,
          buyer_nick: feedback_dto.buyer.nick,
          buyer_head: feedback_dto.buyer.head,
          seller_id: feedback_dto.seller.id,
          seller_nick: feedback_dto.seller.nick,
          seller_head: feedback_dto.seller.head,
          rate_num: feedback_dto.rateNum,
          comment: feedback_dto.comment,
          sell_side: feedback_dto.sellSide,
          thread_id: feedback_dto.threadId,
          entry_id: feedback_dto.entryId,
          buyer_status: buyer_status,
          seller_status: seller_status
      )
      Rails.logger.info("feedback_id: #{feedback.id}, sell_side: #{feedback_dto.sellSide}, buyer_status: #{buyer_status}, seller_status: #{seller_status}")
      stage_push(feedback)

      MessageWorker::FeedbackPushWorker.perform_async(feedback.id)
    rescue => e
      p e
      x = FmmcApiError.new
      x.errorCode = 9104
      x.errorMsg = 'send feedback error'
      raise x
    end
  end

  def sendMessage(message_dto)
    begin
      template = Template.find_by_name(message_dto.template)
      if template == nil
        x = FmmcApiError.new
        x.errorCode = 9001
        x.errorMsg = "Template #{message_dto.template} not found"
        raise x
      end
      if message_dto.content == nil
        content = template.content

        if message_dto.placeholders
          message_dto.placeholders.each do |k,v|
            #puts "#{k}, #{v}"
            content = content.gsub("\#\{#{k}\}", v)
          end
        end
      end
      message = Message.create!(
          user_id: message_dto.userId,
          content: content,
          template: template,
          status: Constants::Status::UNREAD
      )

      stage_push(message)

      MessageWorker::MessagePushWorker.perform_async(message.id)
    rescue => e
      p e
      x = FmmcApiError.new
      x.errorCode = 9105
      x.errorMsg = 'send message error'
      raise x
    end
  end

  def send_mail(target_user, template = nil, text = nil, data = nil, subject = nil)
    begin
      if !template.blank?
        mail_template = MailTemplate.find_by_name(template)
        if mail_template == nil
          x = FmmcApiError.new
          x.errorCode = 9001
          x.errorMsg = "Mail template #{template} not found"
          p x
          raise x
        elsif mail_template.status == 0
          p "Mail template #{template} disabled"
          return
        end
      elsif text.blank? && subject.blank?
        x = FmmcApiError.new
        x.errorCode = 9001
        x.errorMsg = "Both text and subject are required."
        p x
        raise x
      end

      MailWorker.perform_async(target_user, template, text, data, subject)
    rescue => e
      x = FmmcApiError.new
      x.errorCode = 9001
      x.errorMsg = e
      p x
      raise x
    end
  end

  def push(target_user, template = nil, text = nil, data = nil, payload = nil)
    begin
      if !template.blank?
        push_template = PushTemplate.find_by_name(template)
        if push_template == nil
          x = FmmcApiError.new
          x.errorCode = 9001
          x.errorMsg = "Push template #{template} not found"
          p x
          raise x
        elsif push_template.status == 0
          p "Push template #{template} disabled"
          return
        end
      end

      PushWorker.perform_async(target_user, template, text, data, payload)
    rescue => e
      x = FmmcApiError.new
      x.errorCode = 9001
      x.errorMsg = e
      p x
      raise x
    end
  end

  def notify(target_user, cron_name, data = nil)
    cron = Cron.find_by_name(cron_name)
    if not cron
      p "cron '#{cron_name}' not found, exit"
      x = FmmcApiError.new
      x.errorCode = 9001
      x.errorMsg = "cron '#{cron_name}' not found"
      raise x
    end

    if cron.status == Cron::DISABLED
      p "cron '#{cron_name}' disabled, exit"
      return
    end

    if cron_name == 'CRON_REMIND_TO_LIST'
      # 2天后执行
      RemindToListMessageWorker.perform_at(48.hours.from_now, target_user, cron_name, data)
      # RemindToListMessageWorker.perform_at(5.minute.from_now, target_user, cron_name, data)
      # RemindToListMessageWorker.perform_at(24.hour.from_now, target_user, cron_name, data)
      # RemindToListMessageWorker.perform_async(target_user, cron_name, data)
    elsif cron_name == 'CRON_REMIND_TO_MAKE_OFFER'
      # 澳洲时间中午12点执行
      p "CRON_REMIND_TO_MAKE_OFFER: target_user => #{target_user}"
      RemindToMakeOfferMessageWorker.perform_at(Time.now.tomorrow.to_date + 2.hours, target_user, cron_name, data)
      # RemindToMakeOfferMessageWorker.perform_at(Time.now.to_date + 10.hours, target_user, cron_name, data)
    elsif cron_name == 'CRON_SOLD_OUT_BROADCAST'
      p "CRON_SOLD_OUT_BROADCAST"
      SoldOutBroadcastMessageWorker.perform_async(cron_name, data)
    end
  end

  private

  # 保存在push表中等待推送
  def stage_push(pushable)
    clients = Client.get_valid_clients(pushable.to_id)
    clients.each do |c|
      Push.create(
          client: c,
          pushable: pushable,
          status: Constants::PushStatus::FRESH
      )
    end
  end
end