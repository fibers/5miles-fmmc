namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
=begin
    User.create!(name: "Example User",
                 email: "example@railstutorial.org",
                 password: "foobar",
                 password_confirmation: "foobar")
    50.times do |n|
      name  = Faker::Name.name
      email = "example-#{n+1}@railstutorial.org"
      password  = "password"
      User.create!(name: name,
                   email: email,
                   password: password,
                   password_confirmation: password)
    end
=end

    Template.create!(name: 'ITEM_CAT_CHANGED',
                     message_type: 'ITEM_MAINT_NOTI',
                     content: '#{item_title} was moved to another category: #{cat_title}')
    Template.create!(name: 'SELLER_PUB_RESTRICTED',
                     message_type: 'SELLER_MAINT_NOTI',
                     content: 'you are not allowed to list')

=begin
    Message.create!(
        receiver_id: 1,
        content: 'xxx was moved to another category: yyy',
        template_id: 1
    )
    Message.create!(
        receiver_id: 1,
        content: 'you are not allowed to list',
        template_id: 2
    )

    Question.create!(buyer_id: 1,
                    buyer_nick: 'James',
                    buyer_head: '/image/avatar/james.png',
                    seller_id: 2,
                    seller_nick: 'Henry',
                    seller_head: '/image/avatar/henry.png',
                    content: 'Great! I like this one, and how...',
                    item_id: 456,
                    item_image: '/image/456.png',
                    item_title: 'some toy',
                    is_reply: false)

    Question.create!(buyer_id: 1,
                     buyer_nick: 'James',
                     buyer_head: '/image/avatar/james.png',
                     seller_id: 2,
                     seller_nick: 'Henry',
                     seller_head: '/image/avatar/henry.png',
                     content: 'Yes...',
                     item_id: 456,
                     item_image: '/image/456.png',
                     item_title: 'some toy',
                     is_reply: true)

    Question.create!(buyer_id: 3,
                     buyer_nick: 'Kate',
                     buyer_head: '/image/avatar/kate.png',
                     seller_id: 2,
                     seller_nick: 'Henry',
                     seller_head: '/image/avatar/henry.png',
                     content: 'Hello, ...',
                     item_id: 123,
                     item_image: '/image/123.png',
                     item_title: 'Glasses',
                     is_reply: false)

    Question.create!(buyer_id: 3,
                     buyer_nick: 'Kate',
                     buyer_head: '/image/avatar/kate.png',
                     seller_id: 2,
                     seller_nick: 'Henry',
                     seller_head: '/image/avatar/henry.png',
                     content: 'Uh...',
                     item_id: 123,
                     item_image: '/image/123.png',
                     item_title: 'Glasses',
                     is_reply: true)

    Offer.create!(buyer_id: 1,
                  buyer_nick: 'James',
                  buyer_head: '/image/avatar/james.png',
                  seller_id: 2,
                  seller_nick: 'Henry',
                  seller_head: '/image/avatar/henry.png',
                  price: '399.99',
                  comment: 'Could you make it cheaper?',
                  item_id: 456,
                  item_image: '/image/456.png',
                  item_title: 'some toy',
                  entry_id: 123,
                  sell_side: 0,
                  thread_id: 444)
    Offer.create!(buyer_id: 1,
                  buyer_nick: 'James',
                  buyer_head: '/image/avatar/james.png',
                  seller_id: 2,
                  seller_nick: 'Henry',
                  seller_head: '/image/avatar/henry.png',
                  price: '350',
                  comment: nil,
                  item_id: 456,
                  item_image: '/image/456.png',
                  item_title: 'some toy',
                  entry_id: 123,
                  sell_side: 1,
                  thread_id: 444)

    Feedback.create!( buyer_id: 1,
                      buyer_nick: 'James',
                      buyer_head: '/image/avatar/james.png',
                      seller_id: 2,
                      seller_nick: 'Henry',
                      seller_head: '/image/avatar/henry.png',
                      comment: 'You are a nice guy!',
                      item_id: 456,
                      item_image: '/image/456.png',
                      item_title: 'some toy',
                      rate_num: 5,
                      thread_id: 444,
                      sell_side: 0)

    Chat.create!(buyer_id: 1,
                 buyer_nick: 'James',
                 buyer_head: '/image/avatar/james.png',
                 seller_id: 2,
                 seller_nick: 'Henry',
                 seller_head: '/image/avatar/henry.png',
                 content: 'Hi...',
                 item_id: 456,
                 item_image: '/image/456.png',
                 item_title: 'some toy',
                 thread_id: 444,
                 sell_side: 0)

    Chat.create!(buyer_id: 1,
                 buyer_nick: 'James',
                 buyer_head: '/image/avatar/james.png',
                 seller_id: 2,
                 seller_nick: 'Henry',
                 seller_head: '/image/avatar/henry.png',
                 content: 'Sorry...',
                 item_id: 456,
                 item_image: '/image/456.png',
                 item_title: 'some toy',
                 thread_id: 444,
                 sell_side: 1)
=end
  end
end