namespace :cron do
  desc 'promotion_USA_official_3_push'
  task promotion_USA_official_3_push: :environment do
    cron_name = get_cron_name(__FILE__)
    p "cron #{cron_name} executing..."
    cron = Cron.find_by_name(cron_name)
    if not cron
      p "cron '#{cron_name}' not found, exit"
      exit
    end

    if cron.status == Cron::DISABLED
      p "cron '#{cron_name}' disabled, exit"
      exit
    end

    require 'message_worker'
    Round1PushTestWorker.perform_async(cron.task_id)
  end
end
