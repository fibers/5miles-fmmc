def get_cron_name(file)
  filename = File.basename(file)
  "CRON_#{filename.slice(0...filename.index('.')).upcase}"
end

def replace_content(content, data)
  if data
    data.each do |k,v|
      # puts "#{k}, #{v}"
      content = content.gsub("\#\{#{k}\}", v)
    end
  end
  content
end