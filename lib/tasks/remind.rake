require File.expand_path('../cron_utils', __FILE__)
require 'redis'

namespace :cron do
  desc 'reminds user to list'
  task remind_listing: :environment do
    cron_name = get_cron_name(__FILE__)
    p "cron #{cron_name} executing..."
    cron = Cron.find_by_name(cron_name)
    if not cron
      p "cron '#{cron_name}' not found, exit"
      exit
    end

    if cron.status == Cron::DISABLED
      p "cron '#{cron_name}' disabled, exit"
      exit
    end

    # 注册后48小时没有发布商品
    users = User.signup_48h_ago_but_unlist
    if users && users.size > 0
      users.each do |user|
        payload = {} # FIXME use real data to replace the placeholders
        push_payload = {}
        push_content = replace_content(cron.push_template.content, payload)
        MessageWorker.perform_async(user.id, push_content, push_payload)
        mail_template = cron.mail_template
        mail_content = replace_content(mail_template.content, payload) # FIXME use real data to replace the placeholders
        MailWorker.perform_async(mail_template.subject, mail_content, user.email)
      end
    end

    # 已经发布过商品并收到第一个1个Offer
    redis = Redis.new(:host => Settings.biz_redis_host, :port => Settings.biz_redis_port, :db => biz_redis_db)

  end
end