namespace :cron do
  desc 'round 1 test day 2-3'
  task round_1_test_day_2_3: :environment do
    cron_name = get_cron_name(__FILE__)
    p "cron #{cron_name} executing..."
    cron = Cron.find_by_name(cron_name)
    if not cron
      p "cron '#{cron_name}' not found, exit"
      exit
    end

    if cron.status == Cron::DISABLED
      p "cron '#{cron_name}' disabled, exit"
      exit
    end

    require 'message_worker'
    Round1PushWorker.perform_async(cron_name)
  end
end