namespace :cron do
  desc '汇总发送follow的卖家前一天发的新商品'
  task follow_2: :environment do
    cron_name = get_cron_name(__FILE__)
    p "cron #{cron_name} executing..."
    cron = Cron.find_by_name(cron_name)
    if not cron
      p "cron '#{cron_name}' not found, exit"
      exit
    end

    if cron.status == Cron::DISABLED
      p "cron '#{cron_name}' disabled, exit"
      exit
    end

    require 'message_worker'

    PromptFollowersHasNewItemsMessageWorker.perform_async(cron.task_id)
  end
end