#!/usr/bin/env ruby
require 'daemon_spawn'

class ApiServer < DaemonSpawn::Base

  def start(args)
    # process command-line args
    # start your bad self

    # Loads Rails environment
    ENV['RAILS_ENV'] ||= args.first || 'development'
    require File.expand_path('../../config/boot',  __FILE__)
    require File.expand_path('../../config/environment', __FILE__)

    port = Settings.thrift_port || 2048
    thread_count = Settings.thrift_thread || 5

    # Includes thrift-generated code
    $:.push("#{Rails.root}/lib/thrift")
    require 'gen-rb/fmmc_api'
    require 'fmmc_api_handler'

    handler = FmmcApiHandler.new
    processor = FmmcApi::Processor.new(handler)
    transport = Thrift::ServerSocket.new(port)
    transportFactory = Thrift::BufferedTransportFactory.new
    protocolFactory = Thrift::BinaryProtocolFactory.new
    server = Thrift::ThreadPoolServer.new(processor, transport, transportFactory, protocolFactory, thread_count)

    puts 'Started the thrift API server...'
    server.serve
  end

  def stop
    # stop your bad self
    puts 'Stopped the thrift API server...'
  end
end

ApiServer.spawn!(:log_file => '/var/www/fmmc/current/log/api_server.log',
                :pid_file => '/var/www/fmmc/current/tmp/pids/api_server.pid',
                :sync_log => true,
                :working_dir => File.dirname(__FILE__))
