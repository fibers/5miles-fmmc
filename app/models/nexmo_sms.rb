require 'singleton'

class NexmoSms

  include Singleton

  def initialize
  end

  def send(to, content)
    #conn = Faraday.new(:url => Settings.sms.base_url) do |faraday|
    #  faraday.request  :url_encoded             # form-encode POST params
    #  faraday.response :logger                  # log requests to STDOUT
    #  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    #end

    #response = conn.get do |req|
    #  req.url '/sms/json'
    #  req.params['api_key'] = Settings.sms.api_key,
    #  req.params['api_secret'] = Settings.sms.api_secret,
    #  req.params['from'] = Settings.sms.from,
    #  req.params['to'] = to,
    #  req.params['text'] = content,
    #  req.params['status-report-req'] = Settings.sms.status_report_req
    #end

    response = Faraday.get Settings.sms.base_url, { :api_key => Settings.sms.api_key,
                                                    :api_secret => Settings.sms.api_secret,
                                                    :from => Settings.sms.from,
                                                    :to => to,
                                                    :text => content,
                                                    'status-report-req' => Settings.sms.status_report_req }

    JSON.parse response.body
  end
end