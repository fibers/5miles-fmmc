class SystemMessageTemplate < ActiveRecord::Base
  self.table_name = 'fmmc_system_message_template'
  attr_accessible :comment, :content, :name, :status, :image, :action, :desc, :sm_type
end
