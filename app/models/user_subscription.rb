class UserSubscription < ActiveRecord::Base
  self.table_name = 'fmmc_user_subscription'

  NOTIFICATION_TYPE_ALL = 0
  NOTIFICATION_TYPE_NEW_FOLLOWERS = 1
  NOTIFICATION_TYPE_RECOMMENDATIONS = 2
  NOTIFICATION_TYPE_MARKETING_ACTIVITIES = 3
  NOTIFICATION_TYPE_FUNCTIONAL = 4
  NOTIFICATION_TYPE_OFFERS_AND_MESSAGES = 5
  NOTIFICATION_TYPE_REMINDERS = 6

  NOTIFICATION_METHOD_PUSH = 1
  NOTIFICATION_METHOD_EMAIL = 2
  NOTIFICATION_METHOD_SMS = 3

  belongs_to :user
  attr_accessible :notification_method, :notification_type, :state

  def self.subscribed?(user_id, notification_method, notification_type)
    us = where("user_id = ? and notification_method = ? and notification_type in(?, #{NOTIFICATION_TYPE_ALL})",
                user_id, notification_method, notification_type)
    if us.count == 0
      true
    else
      us[0].state
    end
  end
end
