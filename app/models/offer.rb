class Offer < BaseMessage

  OFFER = 1
  CHAT = 2

  attr_accessible :buyer_head, :buyer_id, :buyer_nick, :message, :item_id, :item_image, :item_title,
                  :price, :ref_offer_id, :seller_head, :seller_id,
                  :seller_nick, :sell_side, :thread_id, :entry_id, :accepted, :offer_type

  def self.find_items_by_user(user_id, user_type, exclusive_ids = nil)
    sql = user_sql_clause(user_id, user_type)
    user_prefix = user_prefix(user_type)

    if exclusive_ids && exclusive_ids.length > 0
      find_by_sql('select * from offers where id in
                  (select max(id) from offers where ' + sql +
                  " and item_id not in (#{exclusive_ids.map { |id| "'" + id.to_s + "'" }.join(', ')})
                  group by item_id and #{user_prefix}_id) order by id desc")
    else
      find_by_sql('select * from offers where id in
                  (select max(id) from offers where ' + sql +
                  " group by item_id and #{user_prefix}_id) order by id desc")
    end
  end

  def self.find_recent_by_user_and_item(user_id, user_type, item_id, offer_type)
    sql = user_sql_clause(user_id, user_type)
    sql = "#{sql} and item_id = '#{item_id}' and offer_type = #{offer_type}"
    offers = where(sql).limit(1).order('id DESC')
    if offers.size > 0
      { :type => 'O',
        :offer_type => offers[0].offer_type,
        :price => offers[0].price,
        :message => offers[0].message,
        :from_user_id => offers[0].sell_side == Constants::SellSide::BUYER_TO_SELLER ? offers[0].buyer_id : offers[0].seller_id,
        :from_user_nick => offers[0].sell_side == Constants::SellSide::BUYER_TO_SELLER ? offers[0].buyer_nick : offers[0].seller_nick,
        :thread_id => offers[0].thread_id,
        :accepted => offers[0].accepted?,
        :timestamp => offers[0].created_at.to_i
      }
    else
      {}
    end
  end

  def self.unread_num(user_id, user_type, item_id = nil)
    sql = user_unread_sql_clause(user_id, user_type)
    if item_id
      sql = "#{sql} and item_id = '#{item_id}'"
    end
    where(sql).count
  end

  def to_id
    sell_side == 1 ? buyer_id : seller_id
  end

  def to_mobile_phone
    client = Client.find_by_user_id(to_id)
    client.mobile_phone
  end

  def update_push_result(response)
    update_attributes(  push_error_code: response['result'],
                        push_error_msg: response['status'],
                        push_result: response['result'] == 'ok' ? 1 : 0,
                        pushed_at: Time.now)
  end

  def update_sms_result(response)
    update_attributes(  sms_error_code: response['messages'][0]['status'],
                        sms_error_msg: response['messages'][0]['error-text'],
                        sms_result: response['messages'][0]['status'] == '0' ? 1 : 0,
                        sms_sent_at: Time.now)
  end

  def push_alert
    from = sell_side == 1 ? seller_nick : buyer_nick
    if accepted?
      alert = "#{from} accepts the offer #{price}"
    elsif
      alert = "#{from} offers #{price}"
    end

    if !comment.blank?
      alert = "#{alert}, #{comment}"
    end

    alert
  end

  # 合成push透传内容
  def push_payload
    from = sell_side == 1 ? seller_nick : buyer_nick
    { type: 'O', id: entry_id, thread: thread_id, from: from, price: price, comment: comment, accepted: accepted }.to_json
  end

  # 合成push透传内容
  def push_payload_ios
    { type: 'O', thread: thread_id }.to_json
  end
end
