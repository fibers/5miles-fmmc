class PushTemplate < ActiveRecord::Base

  self.table_name = 'fmmc_push_template'

  attr_accessible :comment, :content, :name, :status, :payload, :title, :flag, :notification_type, :daily_quota
end
