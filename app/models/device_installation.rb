class DeviceInstallation < ActiveRecord::Base
  self.table_name = 'message_deviceinstallation'
  belongs_to :user
  attr_accessible :app_version, :device_id, :installation_id, :os, :provider, :push_token
end
