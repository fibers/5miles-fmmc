require 'json'

class Question < BaseMessage
  attr_accessible :buyer_head, :buyer_id, :buyer_nick, :content, :item_id, :item_image, :item_title,
                  :seller_head, :seller_id, :seller_nick, :is_reply, :entry_id

  def self.find_items_by_user(user_id, user_type)
    sql = user_sql_clause(user_id, user_type)
    user_prefix = user_prefix(user_type)
    sql = "select * from questions where id in
          (select max(id) from questions where #{sql}
          group by item_id, #{user_prefix}_id) order by id desc"
    find_by_sql(sql)
  end

  def self.unread_num(user_id, user_type, item_id = nil)
    sql = user_unread_sql_clause(user_id, user_type)
    if item_id
      sql = "#{sql} and item_id = '#{item_id}'"
    end
    where(sql).count
  end

  def to_id
    is_reply? ? buyer_id : seller_id
  end

  def push_alert
    from = is_reply? ? seller_nick : buyer_nick
    "#{from}: #{content}"
  end

  # 合成push透传内容
  def push_payload
    from = is_reply? ? seller_nick : buyer_nick
    { type: 'Q', item: item_id, from: from, reply: is_reply?, text: content }.to_json
  end

  # 合成push透传内容
  def push_payload_ios
    { type: 'Q', item: item_id }.to_json
  end
end
