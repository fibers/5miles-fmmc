class Trigger < ActiveRecord::Base

  self.table_name = 'fmmc_trigger'

  ENABLED = 1
  DISABLED = 0

  TRIGGER_TYPE_INSTANT = 1
  TRIGGER_TYPE_DELAYED = 2

  belongs_to :frequency_period, :foreign_key => 'interval_period_id'
  belongs_to :task
  attr_accessible :comment, :name, :status, :interval_quantity, :at, :trigger_type

  def interval
    interval_quantity.send(frequency_period.name.pluralize)
  end
end
