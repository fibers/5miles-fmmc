class Message < BaseMessage
  belongs_to :template

  attr_accessible :click_action, :content, :user_id, :message_type, :template, :status

  def self.unread_num(user_id)
    where(
        "user_id = :user_id and status = :status",
        user_id: user_id,
        status: Constants::Status::UNREAD
    ).count
  end

  def to_id
    user_id
  end

  def to_mobile_phone
    client = Client.find_by_user_id(user_id)
    client.mobile_phone
  end

  def update_push_result(response)
    update_attributes(  push_error_code: response['result'],
                        push_error_msg: response['status'],
                        push_result: response['result'] == 'ok' ? 1 : 0,
                        pushed_at: Time.now)
  end

  def update_sms_result(response)
    update_attributes(  sms_error_code: response['messages'][0]['status'],
                        sms_error_msg: response['messages'][0]['error-text'],
                        sms_result: response['messages'][0]['status'] == '0' ? 1 : 0,
                        sms_sent_at: Time.now)
  end

  def push_alert
    content
  end

  # 合成push透传内容
  def push_payload
    { type: 'S', text: content}.to_json
  end

  # 合成push透传内容
  def push_payload_ios
    { type: 'S' }.to_json
  end

  def mark_read
    update_attributes(
        status: Constants::Status::READ
    )
  end

  def mark_deleted
    update_attributes(
        status: Constants::Status::DELETED
    )
  end
end
