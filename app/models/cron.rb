class Cron < ActiveRecord::Base

  self.table_name = 'fmmc_cron'

  ENABLED = 1
  DISABLED = 0

  belongs_to :frequency_period
  belongs_to :task
  attr_accessible :comment, :name, :push_quota,
                  :status, :frequency_quantity, :at, :tz

  # Used by clockwork to schedule how frequently this event should be run
  # Should be the intended number of seconds between executions
  def frequency
    frequency_quantity.send(frequency_period.name.pluralize)
  end
end
