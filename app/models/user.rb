class User < ActiveRecord::Base
  self.table_name = 'entity_user'

  attr_accessible :created_at, :email, :id, :last_login_at, :nickname, :portrait, :country, :is_robot

  has_many :device_installations

  # 查询注册超过48小时未发布商品的用户
  def self.signup_48h_ago_but_unlist
    find_by_sql('SELECT u.id, u.email FROM entity_user u LEFT JOIN entity_item i ON u.id = i.user_id
                WHERE i.id IS NULL AND u.created_at between date_sub(now(), INTERVAL 3 DAY)
                AND date_sub(now(), INTERVAL 2 DAY)')
  end

end
