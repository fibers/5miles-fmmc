class SmsTemplate < ActiveRecord::Base
  self.table_name = 'fmmc_sms_template'
  attr_accessible :comment, :content, :name, :status
end
