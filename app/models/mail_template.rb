class MailTemplate < ActiveRecord::Base

  self.table_name = 'fmmc_mail_template'

  attr_accessible :comment, :content, :name, :status, :subject, :title, :from_address, :notification_type
end
