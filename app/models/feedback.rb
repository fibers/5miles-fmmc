class Feedback < BaseMessage
  attr_accessible :comment, :item_id, :item_image, :item_title,
                  :rate_num, :seller_head, :seller_id, :seller_nick, :buyer_head, :buyer_id, :buyer_nick,
                  :sell_side, :thread_id, :entry_id

  def self.find_recent_by_user_and_item(user_id, user_type, item_id)
    sql = user_sql_clause(user_id, user_type)
    sql = "#{sql} and item_id = '#{item_id}'"
    feedbacks = where(sql).limit(1).order('id DESC')
    if feedbacks.size > 0
      { :type => 'F',
        :rate_num => feedbacks[0].rate_num,
        :message => feedbacks[0].comment,
        :from_user_id => feedbacks[0].sell_side == Constants::SellSide::BUYER_TO_SELLER ? feedbacks[0].buyer_id : feedbacks[0].seller_id,
        :from_user_nick => feedbacks[0].sell_side == Constants::SellSide::BUYER_TO_SELLER ? feedbacks[0].buyer_nick : feedbacks[0].seller_nick,
        :thread_id => feedbacks[0].thread_id,
        :timestamp => feedbacks[0].created_at.to_i
      }
    else
      {}
    end
  end

  def self.unread_num(user_id, user_type, item_id = nil)
    sql = user_unread_sql_clause(user_id, user_type)
    if item_id
      sql = "#{sql} and item_id = '#{item_id}'"
    end
    where(sql).count
  end

  def to_id
    sell_side == 1 ? buyer_id : seller_id
  end

  def to_mobile_phone
    client = Client.find_by_user_id(to_id)
    client.mobile_phone
  end

  def update_push_result(response)
    update_attributes(  push_error_code: response['result'],
                        push_error_msg: response['status'],
                        push_result: response['result'] == 'ok' ? 1 : 0,
                        pushed_at: Time.now)
  end

  def update_sms_result(response)
    update_attributes(  sms_error_code: response['messages'][0]['status'],
                        sms_error_msg: response['messages'][0]['error-text'],
                        sms_result: response['messages'][0]['status'] == '0' ? 1 : 0,
                        sms_sent_at: Time.now)
  end

  def push_alert
    from = sell_side == 1 ? seller_nick : buyer_nick
    alert = "#{from} left you feedback: #{rate_num} points"
    if !comment.blank?
      alert = "#{alert}, #{comment}"
    end
    alert
  end

  # 合成push透传内容
  def push_payload
    from = sell_side == 1 ? seller_nick : buyer_nick
    { type: 'F', id: entry_id, thread: thread_id, from: from, rate: rate_num, comment: comment}.to_json
  end

  # 合成push透传内容
  def push_payload_ios
    { type: 'F', thread: thread_id }.to_json
  end
end
