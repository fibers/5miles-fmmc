class Template < ActiveRecord::Base

  MESSAGE_TYPES = { ITEM_MAINT_NOTI: 'ITEM_MAINT_NOTI', SELLER_MAINT_NOTI: 'SELLER_MAINT_NOTI' }

  attr_accessible :content, :name, :message_type

  before_save {
    name.upcase!
    message_type.upcase!
  }

  validates :name,  presence: true, length: { maximum: 50 },
            uniqueness: { case_sensitive: false }
  validates :message_type,  presence: true, length: { maximum: 50 }
  validates :content,  presence: true, length: { maximum: 250 }
end
