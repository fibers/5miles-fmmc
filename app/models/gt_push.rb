$:.unshift(File.dirname(__FILE__) + "../../../lib/push/igetui")
require 'net/http'
require 'uri'
require 'json'
require 'digest/md5'
require 'template/transmission_template'
require 'template/notification_template'
require 'igt_message'
require 'igt_push'
require 'base64'
require 'singleton'

class GtPush

  include Singleton

  URL = 'http://sdk.open.api.igexin.com/apiex.htm'

  def push_noti_to_single(content, client_id, title, text)
    iGtPush = GtReq::IGtPush.new(URL, Settings.getui.app_key, Settings.getui.master_secret)
    notificationTemplate = GtReq::NotificationTemplate.new
    notificationTemplate.appId= Settings.getui.app_id
    notificationTemplate.appKey = Settings.getui.app_key
    notificationTemplate.logo = 'push.png'
    notificationTemplate.logoURL = 'http://www.igetui.com/wp-content/uploads/2013/08/logo_getui1.png'
    notificationTemplate.title = title
    notificationTemplate.text = text
    notificationTemplate.isClearable = false
    notificationTemplate.isRing = true
    notificationTemplate.isVibrate = true
    notificationTemplate.transmissionType = 1
    notificationTemplate.transmissionContent = content

    singleMessage = GtReq::SingleMessage.new
    singleMessage.data = notificationTemplate
    singleMessage.isOffline = true
    singleMessage.offlineExpireTime = 6 * 1000 #ms
    target = GtReq::Target.new
    target.appId = Settings.getui.app_id
    target.clientId = client_id
    ret = iGtPush.pushMessageToSingle(singleMessage, target)
    # p ret
  end

  def push_to_list(payload, client_ids, badge, alert, sound)
    iGtPush = GtReq::IGtPush.new(URL, Settings.getui.app_key, Settings.getui.master_secret)
    transmissionTemplate = GtReq::TransmissionTemplate.new
    transmissionTemplate.appId = Settings.getui.app_id
    transmissionTemplate.appKey = Settings.getui.app_key
    transmissionTemplate.transmissionType = 2
    transmissionTemplate.transmissionContent = payload
    transmissionTemplate.setPushInfo('', badge, alert, sound, payload, '', '', '')
    listMessage = GtReq::ListMessage.new
    listMessage.data = transmissionTemplate
    listMessage.isOffline = true
    listMessage.offlineExpireTime = 1
    targetList = []
    client_ids.each do |client_id|
      target = GtReq::Target.new
      target.appId = Settings.getui.app_id
      target.clientId = client_id
      targetList << target
    end

    contentId = iGtPush.getContentId(listMessage)
    ret = iGtPush.pushMessageToList(contentId, targetList)
    # p ret
  end

  def push_to_app(content)
    iGtPush = GtReq::IGtPush.new(URL, Settings.getui.app_key, Settings.getui.master_secret)
    transmissionTemplate = GtReq::TransmissionTemplate.new
    transmissionTemplate.appId = Settings.getui.app_id
    transmissionTemplate.appKey = Settings.getui.app_key
    transmissionTemplate.transmissionType = 2
    transmissionTemplate.transmissionContent = content

    appMessage = GtReq::AppMessage.new
    appMessage.data = transmissionTemplate
    appMessage.isOffline = false
    appMessage.offlineExpireTime = 0
    appMessage.appIdList = [Settings.getui.app_id]
    appMessage.provinceList = ['浙江', '上海']
    #appMessage.tagList = ['开心']

    ret = iGtPush.pushMessageToApp(appMessage)
    # p ret
  end

  def status_test(client_id)
    iGtPush = GtReq::IGtPush.new(URL, Settings.getui.app_key, Settings.getui.master_secret)
    p iGtPush.getClientIdStatus(Settings.getui.app_id, client_id)
  end

  def push_to_single(payload, client_id, badge, alert, sound, os)
    # p "#{Settings.getui.app_key}, #{Settings.getui.master_secret}"
    iGtPush = GtReq::IGtPush.new(URL, Settings.getui.app_key, Settings.getui.master_secret)
    transmissionTemplate = GtReq::TransmissionTemplate.new
    transmissionTemplate.appId = Settings.getui.app_id
    transmissionTemplate.appKey = Settings.getui.app_key
    transmissionTemplate.transmissionType = 2
    transmissionTemplate.transmissionContent = payload
    singleMessage = GtReq::SingleMessage.new
    os.downcase!
    if os == Client::OS_TYPE_IOS
      #setPushInfo(actionLocKey, badge, message, sound, payload, locKey, locArgs, launchImage)
      transmissionTemplate.setPushInfo('', badge, alert, sound, payload, '', '', '')
      singleMessage.offlineExpireTime = 1
    elsif os == Client::OS_TYPE_ANDROID
      singleMessage.offlineExpireTime = 24 * 3600 * 1000 # 离线存储24小时
    end

    singleMessage.data = transmissionTemplate
    singleMessage.isOffline = true
    target = GtReq::Target.new
    target.appId = Settings.getui.app_id
    target.clientId = client_id
    ret = iGtPush.pushMessageToSingle(singleMessage, target)
    # p ret
  end
end