class BaseMessage < ActiveRecord::Base

  has_many :pushes, as: :pushable

  def self.user_sql_clause(user_id, user_type)
    user_prefix = user_prefix(user_type)
    "#{user_prefix}_id = '#{user_id}' and #{user_prefix}_status != #{Constants::Status::DELETED}"
  end

  def self.user_prefix(user_type)
    if user_type == 'buyer'
      prefix = 'buyer'
    elsif user_type == 'seller'
      prefix = 'seller'
    end
    prefix
  end

  def self.user_unread_sql_clause(user_id, user_type)
    user_prefix = user_prefix(user_type)
    "#{user_prefix}_id = '#{user_id}' and #{user_prefix}_status = #{Constants::Status::UNREAD}"
  end

  self.abstract_class = true

  attr_accessible :buyer_status, :seller_status

  def to_id
    sell_side == 1 ? buyer_id : seller_id
  end

  def to_mobile_phone
    client = Client.find_by_user_id(to_id)
    client.mobile_phone
  end

  def update_push_result(response)
    update_attributes(  push_error_code: response['result'],
                        push_error_msg: response['status'],
                        push_result: response['result'] == 'ok' ? 1 : 0,
                        pushed_at: Time.now)
  end

  def update_sms_result(response)
    update_attributes(  sms_error_code: response['messages'][0]['status'],
                        sms_error_msg: response['messages'][0]['error-text'],
                        sms_result: response['messages'][0]['status'] == '0' ? 1 : 0,
                        sms_sent_at: Time.now)
  end

  def mark_read(user_id)
    if user_id == buyer_id
      update_attributes(
          buyer_status: Constants::Status::READ
      )
    elsif user_id == seller_id
      update_attributes(
          seller_status: Constants::Status::READ
      )
    end
  end

  def mark_deleted(user_id)
    if user_id == buyer_id
      update_attributes(
          buyer_status: Constants::Status::DELETED
      )
    elsif user_id == seller_id
      update_attributes(
          seller_status: Constants::Status::DELETED
      )
    end
  end
end