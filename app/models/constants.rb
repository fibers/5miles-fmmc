class Constants

  module Status
    READ = 1
    UNREAD = 0
    DELETED = -1
    VALUE_MAP = { 1 => 'READ', 0 => 'UNREAD', -1 => 'DELETED' }
    VALID_VALUES = Set.new([READ, UNREAD, DELETED]).freeze
  end

  module PushStatus
    FRESH = 0     # 未发送
    SUCCESS = 1   # 发送成功
    FAILURE = 2   # 发送失败
    KEY_MAP = { 'FRESH' => 0, 'SUCCESS' => 1, 'FAILURE' => 2 }
    VALUE_MAP = { 0 => 'FRESH', 1 => 'SUCCESS', 2 => 'FAILURE' }
    VALID_VALUES = Set.new([FRESH, SUCCESS, FAILURE]).freeze
  end

  module SellSide
    BUYER_TO_SELLER = 0
    SELLER_TO_BUYER = 1
    VALUE_MAP = { 0 => 'BUYER_TO_SELLER', 1 => 'SELLER_TO_BUYER' }
    VALID_VALUES = Set.new([BUYER_TO_SELLER, SELLER_TO_BUYER]).freeze
  end

  module UserType
    BUYER = 'buyer'
    SELLER = 'seller'
  end
end