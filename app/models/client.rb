class Client < ActiveRecord::Base

  OS_TYPE_ANDROID = 'android'
  OS_TYPE_IOS = 'ios'

  module Status
    BIND = 1
    UNBIND = 0
    KEY_MAP = { 'Bind' => 1, 'UNBIND' => 0 }
    VALUE_MAP = {1 => "BIND", 0 => "UNBIND"}
    VALID_VALUES = Set.new([BIND, UNBIND]).freeze
  end

  attr_accessible :client_id, :user_id, :os, :os_version, :mobile_phone, :status

  def self.get_valid_clients(user_id)
    where('user_id = :user_id and status = :status', user_id: user_id, status: Status::BIND)
  end

end
