class Push < ActiveRecord::Base
  belongs_to :client
  belongs_to :pushable, :polymorphic => true
  belongs_to :question, foreign_key: 'pushable_id', conditions: "pushes.pushable_type = 'Question'"

  attr_accessible :error_code, :error_msg, :pushed_at, :status, :pushable, :client

  def update_push_result(response)
    update_attributes(
        error_code: response['result'],
        error_msg: response['status'],
        status: response['result'] == 'ok' ? 1 : 2,
        pushed_at: Time.now
    )
  end
end
