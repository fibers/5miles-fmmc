class Task < ActiveRecord::Base
  self.table_name = 'fmmc_task'
  belongs_to :push_template
  belongs_to :mail_template
  belongs_to :system_message_template
  belongs_to :frequency_period, :foreign_key => 'target_register_interval_period_id'

  attr_accessible :clazz, :name, :status, :need_mail, :need_push, :comment, :target_country, :target_region,
                  :target_city, :target_register_interval_quantity, :target_gender, :target_register_interval_type,
                  :target_uid_tail

  def target_register_interval
    target_register_interval_quantity.send(frequency_period.name.pluralize)
  end
end
