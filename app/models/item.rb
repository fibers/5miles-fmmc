class Item < ActiveRecord::Base
  self.table_name = 'entity_item'

  belongs_to :user
  attr_accessible :created_at, :id, :title, :user
end
