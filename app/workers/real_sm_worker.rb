class RealSmWorker

  include Sidekiq::Worker
  # sidekiq_options :backtrace => true
  # sidekiq_options :queue => :critical
  sidekiq_options :retry => false if Rails.env.development?
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  def initialize
    @redis = Redis.new(:host => "#{Settings.biz_redis_host}", :port => Settings.biz_redis_port, :db => Settings.biz_redis_db)
  end

  def perform(target_user, text, options = {})
    timestamp = Time.now.to_i
    inbox_key = "user:#{target_user}:inbox:system"
    msg_id = @redis.incr('global:next_message_id')
    h = {id: msg_id, to: target_user, text: text, unread: TRUE, timestamp: timestamp, type: 'system'}

    if options['action'].present?
      h[:action] = options['action']
    end
    if options['image'].present?
      h[:image] = options['image']
    end
    if options['desc'].present?
      h[:desc] = options['desc']
    end
    if options['image_type'].present?
      h[:image_type] = options['image_type']
    end
    if options['sm_type'].present?
      h[:sm_type] = options['sm_type']
    end
    if options['review_score'].present?
      h[:review_score] = options['review_score']
    end

    @redis.pipelined do
      @redis.set("#{inbox_key}:new_count", 1)
      @redis.mapped_hmset("message:#{msg_id}", h)
      @redis.zadd(inbox_key, timestamp, msg_id)
      @redis.expire("message:#{msg_id}", 7 * 24 * 60 * 60)  # 7天以后自动过期
    end

    logger.info "[SM] target_user => #{target_user}, text => #{text}, options => #{options}"
  end
end
