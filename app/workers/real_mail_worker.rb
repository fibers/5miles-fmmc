class RealMailWorker

  include Sidekiq::Worker
  # sidekiq_options :backtrace => true
  sidekiq_options throttle: { threshold: 15, period: 1.second }
  # sidekiq_options :queue => :critical
  sidekiq_options :retry => false if Rails.env.development?
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  @@environments = Settings.email_enabled_env

  def initialize
    @redis = Redis.new(:host => "#{Settings.biz_redis_host}", :port => Settings.biz_redis_port, :db => Settings.biz_redis_db)

    @esp = Settings.email_service_provider
    if @esp == 'ses'
      @ses = AWS::SES::Base.new(
          :access_key_id     => Settings.aws.access_key_id,
          :secret_access_key => Settings.aws.secret_access_key,
          :server => Settings.aws.ses_region_endpoint
      )
    elsif @esp == 'mailgun'
      # p "mailgun: base_url => #{Settings.mailgun.base_url}"
    else
      Raven.capture_message('Unknown email service provider, only support ses/mailgun')
      Raise 'Unknown email service provider, only support ses/mailgun'
    end

    @current_environment = ENV['RAILS_ENV'] || ENV['RACK_ENV'] || 'default'
  end

  def resolve_domain(from)
    domain = from.rpartition('@')[2]
    domain.slice!('>')
    domain
  end

  def perform(target_user, subject, text, from, to_email = nil)
    if target_user.present?
      user = @redis.hmget("user:#{target_user}", 'nickname', 'email')

      if not user or user.length == 0 or user[1].blank?
        logger.error "User[id => #{target_user}] not found or email not provided"
        return
      end

      # to = "\"#{user[0]}\" <#{user[1]}>"
      to = user[1]
    elsif to_email.present?
      to = to_email
    else
      return
    end

    if from.blank?
      from = Settings.aws.from_email
    end

    if @esp == 'ses'
      if send_in_current_environment?
        @ses.send_email(
            :to => [to],
            :source => from,
            :subject => subject,
            :html_body => text
        )
      else
        log_excluded_environment_message
      end
    elsif @esp == 'mailgun'
      domain = resolve_domain(from)
      # 用Multimap会报400
      # data = Multimap.new
      # data[:from] = from
      # data[:to] = to
      # data[:subject] = subject
      # data[:html] = text
      # data['h:Reply-To'] = Settings.reply_to_email
      if send_in_current_environment?
        testmode = false
      else
        testmode = true
        log_excluded_environment_message
      end

      response = RestClient.post "https://api:#{Settings.mailgun.api_key}@#{Settings.mailgun.base_url}/#{domain}/messages",
                                 :from => from,
                                 :to => to,
                                 :subject => subject,
                                 :html => text,
                                 'o:testmode' => testmode

      # response = RestClient.post "https://api:#{Settings.mailgun.api_key}@#{Settings.mailgun.base_url}/#{domain}/messages", data

      if response.code != 200
        logger.info "MAIL ERROR: subject => #{subject}, from => #{from}, to => #{to}, status_code => #{response.code}"
        Raven.extra_context(:subject => subject, :from => from, :to => to, :status_code => response.code)
        Raven.capture_message('MAIL ERROR!!!')
      end
    end

    logger.info "[MAIL] subject => #{subject}, from => #{from}, to => #{to}"
  end

  def send_or_skip
    if send_in_current_environment?
      yield
    else
      log_excluded_environment_message
    end
  end

  def send_in_current_environment?
    if @@environments
      @@environments.include?(@current_environment)
    else
      !%w[test cucumber development].include?(@current_environment)
    end
  end

  def log_excluded_environment_message
    logger.info "Email not sent due to excluded environment: #{@current_environment}"
  end
end