class GroupPushWorker

  include Sidekiq::Worker
  # sidekiq_options :backtrace => true
  sidekiq_options :queue => :marketing, throttle: { threshold: 2000, period: 5.minutes }
  sidekiq_options :retry => false if Rails.env.development?
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  def initialize
    # @gt_pusher = IGeTui.pusher(Settings.getui.app_id, Settings.getui.app_key, Settings.getui.master_secret)
    @gt_push = GtPush.instance
  end

  def perform(provider, installations, text, payload = nil, flag = nil, task_id = -1)
    installations = installations.to_set.to_a
    if provider == 'getui'
      return do_getui_group_push(provider, installations, text, payload, flag, task_id)
    end
  end

  def do_getui_group_push(provider, installations, text, payload = nil, flag = nil, task_id = -1)
    if flag == 2
      sound = 'mute.wav'
    else
      sound = '5miles.wav'
    end
    
    if payload.is_a?(String)
      payload_dict = JSON.parse(payload)
    else
      payload_dict = payload.clone
    end

    # 创建一条透传消息
    # template = IGeTui::TransmissionTemplate.new
    # # 设置iOS的推送参数，如果只有安卓 App，可以忽略下面一行
    # template.set_push_info('', 1, text, sound, :payload => payload.to_json)
    #
    # # 创建群发消息
    # list_message = IGeTui::ListMessage.new
    # list_message.data = template
    #
    # # 创建客户端对象
    # client_list = []
    # installations.each do |installation|
    #   client_list << IGeTui::Client.new(installation)
    # end
    #
    # content_id = @gt_pusher.get_content_id(list_message)
    # result = @gt_pusher.push_message_to_list(content_id, client_list)

    # 由于Android客户端需要txt字段作为通知标题，而群发时不区分终端是Android还是iOS，所以此处加入该字段，注意该字段的加入可能导致iOS内容超长
    payload_dict['txt'] = text
    result = @gt_push.push_to_list(payload_dict.to_json, installations, '1', text, sound)
    logger.info "[PUSH] group => true, task_id => #{task_id}, provider => #{provider}, installation_id => #{installations}, result => #{result}, payload => #{payload}, text => #{text}"
  end
end
