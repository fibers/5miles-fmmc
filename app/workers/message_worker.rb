require 'redis'
require 'render_anywhere'
require 'json'

class BaseMessageWorker

  include RenderAnywhere
  include Sidekiq::Worker
  sidekiq_options :retry => false
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  def initialize
    @redis = Redis.new(:host => "#{Settings.biz_redis_host}", :port => Settings.biz_redis_port, :db => Settings.biz_redis_db)
  end

  protected

  def render_to_string(content, data = nil)
    if content.index('<%')
      render :inline => content, :locals => data ? data.symbolize_keys : nil
    else
      content
    end
  end

  def insert_user_token_to_vars(target_user, vars)
    return if target_user.blank? || !vars

    user_token = JWT.encode({'user_id' => target_user.to_s}, Settings.jwt_secret)
    vars[:user_token] = user_token
  end

  def resolve_domain(from)
    domain = from.rpartition('@')[2]
    domain.slice!('>')
    domain
  end

  def insert_domain_to_vars(from, vars)
    vars[:domain] = resolve_domain(from)
  end

  def generate_built_in_vars(vars, user_id = nil, item_id = nil)
    if user_id.present?
      user_info = @redis.hmget("user:#{user_id}", 'id', 'nickname', 'portrait')
      user_token = JWT.encode({'user_id' => user_id.to_s}, Settings.jwt_secret)
      vars[:fuzzy_user_id] = user_info[0]
      vars[:user_nickname] = user_info[1]
      vars[:user_portrait] = user_info[2]
      vars[:user_token] = user_token
    end

    if item_id.present?
      item_info = @redis.hmget("item:#{item_id}", 'id', 'title', 'images', 'local_price')
      vars[:fuzzy_item_id] = item_info[0]
      vars[:item_title] = item_info[1]
      vars[:item_image] = JSON.load(item_info[2])[0]['imageLink']
      vars[:item_price] = item_info[3]
    end
  end

  def is_disabled?(user_id, data = nil)
    user = @redis.hmget("user:#{user_id}", 'email', 'is_robot', 'state')

    if (user[1] && user[1].to_i == 1) ||
        (user[0] && user[0].start_with?('robert_')) ||
        (user[0] && user[0].end_with?('@worldbuz.com')) ||
        (user[0] && user[0].end_with?('@niu.la')) ||
        (user[2] && user[2].to_i != 1 && (!data || !data[:allow_disabled]))
      logger.info "User [id => #{user_id}, email => #{user[0]}, state => #{user[2]}] is robot or disabled, ignore it."
      return true
    end

    return false
  end

  def is_blocked?(source_user, target_user)
    key = "user:#{target_user}:blacklist"
    if @redis.sismember(key, source_user)
      return true
    else
      return false
    end
  end

  # 组合发送
  def send_message(target_user, task, vars, group = false, source_user = nil)
    vars.symbolize_keys!
    return if is_disabled?(target_user, vars)
    return if source_user && is_blocked?(source_user, target_user)

    # 为减少内存占用，群发暂时不发送系统消息
    if !group
      smt = task.system_message_template
      if smt && smt.status == 1
        text = render_to_string(smt.content, vars)
        image = render_to_string(smt.image, vars)
        action = render_to_string(smt.action, vars)
        desc = render_to_string(smt.desc, vars)
        # iamge_type: 0 => item image, 1 => user portrait
        options = {image: image, action: action, desc: desc, sm_type: smt.sm_type,
                   image_type: vars[:image_type], review_score: vars[:review_score]}
        if !group
          RealSmWorker.perform_async(target_user, text, options)
        else
          # 群发类消息一般是营销性消息，走权重较低的marketing队列
          Sidekiq::Client.push('class' => RealSmWorker, 'queue' => 'marketing', 'args' => [target_user, text, options], 'retry' => true, 'backtrace' => true)
        end
      end
    end

    push_template = task.push_template
    if push_template && push_template.status == 1 && UserSubscription.subscribed?(target_user,
                                                                                  UserSubscription::NOTIFICATION_METHOD_PUSH,
                                                                                  push_template.notification_type)
      text = render_to_string(push_template.content, vars)
      title = render_to_string(push_template.title, vars)
      payload = render_to_string(push_template.payload, vars)
      if !group
        RealPushWorker.perform_async(target_user, text, payload, title, push_template.flag, push_template.id, push_template.daily_quota, group, task.id)
      else
        Sidekiq::Client.push('class' => RealPushWorker, 'queue' => 'marketing', 'args' => [target_user, text, payload, title, push_template.flag, push_template.id, push_template.daily_quota, group, task.id], 'retry' => true, 'backtrace' => true)
      end
    end

    mail_template = task.mail_template
    if mail_template && mail_template.status == 1 && UserSubscription.subscribed?(target_user,
                                                                                  UserSubscription::NOTIFICATION_METHOD_EMAIL,
                                                                                  mail_template.notification_type)
      insert_user_token_to_vars(target_user, vars)
      insert_domain_to_vars(mail_template.from_address, vars)
      text = render_to_string(mail_template.content, vars)
      subject = vars[:mail_subject].present? ? vars[:mail_subject] : render_to_string(mail_template.subject, vars)
      if !group
        RealMailWorker.perform_async(target_user, subject, text, mail_template.from_address)
      else
        Sidekiq::Client.push('class' => RealMailWorker, 'queue' => 'marketing', 'args' => [target_user, subject, text, mail_template.from_address], 'retry' => true, 'backtrace' => true)
      end
    end
  end
end

############################### 简单的只具备单一功能的worker ##################################

class PushWorker < BaseMessageWorker
  def perform(target_user, template = nil, text = nil, data = nil, payload = nil, title = nil, flag = nil,
              source_user = nil)
    return if !text && !template
    return if is_disabled?(target_user, data)
    return if is_blocked?(source_user, target_user)

    data ||= {}
    if text.blank?
      push_template = PushTemplate.find_by_name(template)
      text = render_to_string(push_template.content, data)
      title ||= push_template.title
      title = render_to_string(title, data)
      flag ||= push_template.flag
    end

    # since 2.1
    if payload.blank?
      push_template = PushTemplate.find_by_name(template)
      payload = render_to_string(push_template.payload, data)
      title ||= push_template.title
      title = render_to_string(title, data)
      flag ||= push_template.flag
    end

    if push_template && !UserSubscription.subscribed?(target_user,
                                                      UserSubscription::NOTIFICATION_METHOD_PUSH,
                                                      push_template.notification_type)
      logger.info "user##{target_user} unsubscribed, return"
      return
    end

    RealPushWorker.perform_async(target_user, text, payload, title, flag)
  end
end

class MailWorker < BaseMessageWorker
  def perform(target_user, template = nil, text = nil, data = nil, subject = nil, from = nil)
    return if is_disabled?(target_user, data)

    data ||= {}
    if template.present?
      mail_template = MailTemplate.find_by_name(template)
      from ||= mail_template.from_address
      insert_user_token_to_vars(target_user, data)
      insert_domain_to_vars(from, data)
      text ||= render_to_string(mail_template.content, data)
      subject ||= mail_template.subject
      subject = render_to_string(subject, data)
    elsif text.blank? && subject.blank?
      logger.error 'text and subject are required'
      return
    end

    if mail_template && !UserSubscription.subscribed?(target_user,
                                                      UserSubscription::NOTIFICATION_METHOD_EMAIL,
                                                      mail_template.notification_type)
      logger.info "user##{target_user} unsubscribed, return"
      return
    end

    RealMailWorker.perform_async(target_user, subject, text, from)
  end
end

class DirectMailWorker < BaseMessageWorker
  def perform(to, template = nil, text = nil, data = nil, subject = nil, from = nil)
    data ||= {}
    if template.present?
      mail_template = MailTemplate.find_by_name(template)
      from ||= mail_template.from_address
      insert_domain_to_vars(from, data)
      text ||= render_to_string(mail_template.content, data)
      subject ||= mail_template.subject
      subject = render_to_string(subject, data)
    elsif text.blank? && subject.blank?
      logger.error 'text and subject are required'
      return
    end

    RealMailWorker.perform_async(nil, subject, text, from, to)
  end
end

class SmsWorker < BaseMessageWorker
  def perform(target_user, mobile_phone = nil, template = nil, text = nil, data = nil)
    return if target_user.present? && is_disabled?(target_user, data)

    if target_user.present? && mobile_phone.blank?
      mobile_phone = @redis.hget("user:#{target_user}", 'mobile_phone')
    end

    if mobile_phone.blank?
      logger.info "user##{target_user} mobile phone not found, return"
      return
    end

    data ||= {}
    if template.present?
      sms_template = SmsTemplate.find_by_name(template)
      text ||= render_to_string(sms_template.content, data)
    end

    if sms_template && !UserSubscription.subscribed?(target_user,
                                                      UserSubscription::NOTIFICATION_METHOD_SMS,
                                                      UserSubscription::NOTIFICATION_TYPE_REMINDERS)
      logger.info "user##{target_user} unsubscribed, return"
      return
    end

    RealSmsWorker.perform_async(target_user, mobile_phone, text)
  end
end

class SystemMessageWorker < BaseMessageWorker
  def perform(target_user, template = nil, text = nil, data = nil, options = {})
    return if is_disabled?(target_user, data)

    return if !text && !template
    options.symbolize_keys!
    image = options[:image]
    action = options[:action]
    desc = options[:desc]
    sm_type = options[:sm_type]
    image_type = options[:image_type]
    review_score = options[:review_score]

    data ||= {}
    if template.present?
      smt = SystemMessageTemplate.find_by_name(template)
      text ||= smt.content
      image ||= smt.image
      action ||= smt.action
      desc ||= smt.desc
      text = render_to_string(text, data)
      image = render_to_string(image, data)
      action = render_to_string(action, data)
      desc = render_to_string(desc, data)
    end

    options = {image: image, action: action, desc: desc, sm_type: sm_type,
               image_type: image_type, review_score: review_score}
    RealSmWorker.perform_async(target_user, text, options)
  end
end


############################### 可通用的worker ##################################

# 消息群发
class GroupMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    conditions = {:is_robot => 0, :is_anonymous => 0, :state => 1}
    vars = options['data']
    vars ||= {}

    if options['target_user'].present?
      conditions[:id] = options['target_user']
    else
      if task.target_country.present?
        if Settings.INTERNAL_USERS_STRING == task.target_country
          conditions[:id] = Settings.INTERNAL_USERS.values
        else
          conditions[:country] = task.target_country.split(',')
        end
      end

      if task.target_region.present?
          conditions[:region] = task.target_region.split(',')
      end

      if task.target_city.present?
          conditions[:city] = task.target_city.split(',')
      end

      if task.target_gender.present?
        conditions[:gender] = task.target_gender.split(',')
      end

      if task.target_register_interval_quantity.present? && task.frequency_period.present?
        # 0: ago, 1: in
        if task.target_register_interval_type == 0
          conditions[:created_at] = 50.years.ago..task.target_register_interval.send(:ago)
        elsif task.target_register_interval_type == 1
          conditions[:created_at] = task.target_register_interval.send(:ago)..Time.now
        end
      end
    end
    # p conditions
    # User.find_each(:conditions => conditions) do |user|
    #   vars[:nickname] = user.nickname
    #   send_message(user.id, task, vars, true)
    # end

    push_template = task.push_template
    text = render_to_string(push_template.content, vars)
    # title = render_to_string(push_template.title, vars)
    payload = render_to_string(push_template.payload, vars)

    subquery = UserSubscription.where({:notification_method => UserSubscription::NOTIFICATION_METHOD_PUSH, :notification_type => UserSubscription::NOTIFICATION_TYPE_RECOMMENDATIONS, :state => 0}).select(:user_id).uniq
    ids = User.where("id NOT in (#{subquery.to_sql})").where(conditions)
    if task.target_uid_tail.present?
      ids = ids.where("RIGHT(id, 1) in (#{task.target_uid_tail.split(',').map { |e| "'" + e.to_s + "'" }.join(',')})")
    end
    ids = ids.order('id DESC').pluck(:id)

    ids.in_groups_of(50, false).each do |group_ids|
      installation_groups = {}
      User.select('id').where(id: group_ids).includes(:device_installations).order('id DESC').each do |user|
        # p "#{user.id}, #{user.device_installations}"
        user.device_installations.each do |installation|
          provider = installation.provider
          provider_installations = installation_groups[provider]
          if not provider_installations
            installation_groups[provider] = []
          end
          installation_groups[provider] << installation.installation_id
        end
      end
      logger.info "batch#{installation_groups}"
      installation_groups.each do |provider, installations|
        Sidekiq::Client.push('class' => GroupPushWorker, 'queue' => 'marketing', 'args' => [provider, installations, text, payload, push_template.flag, task.id], 'retry' => true, 'backtrace' => true)
      end
    end
  end
end

# 消息单发
class SingleMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    target_user = options['target_user']
    vars = options['data']
    vars ||= {}
    send_message(target_user, task, vars, false, options['source_user'])
  end
end

############################### 为具体业务场景定制的worker ##################################

# 注册后48小时没有发布商品；
class RemindToListMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})

    task = Task.find(task_id)
    return if task.status == 0

    target_user = options['target_user']
    vars = options['data']
    vars ||= {}
    len = @redis.llen("user:#{target_user}:selling_list")
    if len > 0
      logger.info "user[id => #{target_user}] has already posted #{len} #{'item'.pluralize(len)}"
      return
    end

    send_message(target_user, task, vars)
  end
end

# 已经发布过商品并收到第一个1个Offer
class EncourageListing1MessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    target_user = options['target_user']
    vars = options['data']
    item_id = vars['item_id']

    # p "item_id: #{item_id}, sold_to: #{@redis.get("item:#{item_id}:sold_to")}"
    if item_id && @redis.get("item:#{item_id}:sold_to")
      # 该商品是否已经sold
      return
    end

    send_message(target_user, task, vars)
  end
end

# Email用户前一天的followers头像和用户名合集
class Follow1MessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    mail_template = task.mail_template
    return if !mail_template or mail_template.status == 0

    vars = options['data']
    vars ||= {}

    conditions = {:is_robot => 0, :is_anonymous => 0, :state => 1}
    subquery = UserSubscription.where({:notification_method => UserSubscription::NOTIFICATION_METHOD_EMAIL, :notification_type => UserSubscription::NOTIFICATION_TYPE_NEW_FOLLOWERS, :state => 0}).select(:user_id).uniq
    users = User.select('id, nickname').where("email != '' AND id NOT in (#{subquery.to_sql})").where(conditions).order('id DESC')
    users.each do |user|
      follower_ids = @redis.zrangebyscore("user:#{user.id}:followers", 1.day.ago.to_i, Time.now.to_i)
      followers = []
      follower_ids.each do |follower_id|
        follower = @redis.hmget("user:#{follower_id}", 'nickname', 'portrait', 'id')
        followers.push({:follower_id => follower_id, :follower_nickname => follower[0], :follower_portrait => follower[1], :follower_fuzzy_id => follower[2]})
      end

      if followers.length > 0
        vars[:nickname] = user.nickname
        vars[:followers] = followers
        send_message(user.id, task, vars)
      end
    end
  end
end

# 汇总发送follow的卖家前一天发的新商品
class Follow2MessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    mail_template = task.mail_template
    return if !mail_template or mail_template.status == 0

    vars = options['data']
    vars ||= {}

    conditions = {:is_robot => 0, :is_anonymous => 0, :state => 1}
    subquery = UserSubscription.where({:notification_method => UserSubscription::NOTIFICATION_METHOD_EMAIL, :notification_type => UserSubscription::NOTIFICATION_TYPE_RECOMMENDATIONS, :state => 0}).select(:user_id).uniq
    users = User.select('id, nickname').where("email != '' AND id NOT in (#{subquery.to_sql})").where(conditions).order('id DESC')
    users.each do |user|
      following_ids = @redis.zrange("user:#{user.id}:following", 0, -1)
      followings = []
      following_ids.each do |following_id|
        item_ids = @redis.zrangebyscore("user:#{following_id}:user_selling_list", 1.day.ago.to_i, Time.now.to_i)
        if item_ids.length > 0
          following = @redis.hmget("user:#{following_id}", 'nickname', 'portrait', 'id')
          items = []
          item_ids.each do |item_id|
            item_info = @redis.hmget("item:#{item_id}", 'title', 'images', 'local_price', 'id')
            items.push({'item_title' => item_info[0], 'item_image' => JSON.load(item_info[1])[0]['imageLink'], 'item_price' => item_info[2], 'item_id' => item_info[3]})
          end
          followings.push({'following_nickname' => following[0],
                           'following_portrait' => following[1],
                           'following_items' => items,
                           'following_fuzzy_id' => following[2]})
        end
      end

      if followings.length > 0
        vars[:nickname] = user.nickname
        vars[:followings] = followings
        send_message(user.id, task, vars)
      end
    end
  end
end

# Sold时立即给like此商品却没买到的用户
class LikersMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    # 群发该商品其他买家商品已售出
    vars = options['data']
    item_id = vars['item_id']
    # buyer_id = vars['buyer_id']
    item_info = @redis.hmget("item:#{item_id}", 'id', 'title', 'images', 'local_price', 'state')
    vars[:fuzzy_item_id] = item_info[0]
    vars[:item_title] = item_info[1]
    vars[:item_image] = JSON.load(item_info[2])[0]['imageLink']
    vars[:item_price] = item_info[3]
    owner = @redis.get("item:#{item_id}:owner")

    user_ids = Set.new
    # V2.5开始，mark as sold只通知likers，而offer过的买家会收到系统发的假聊天消息
    # offerline_ids = @redis.lrange("item:#{item_id}:offerlines", 0, -1)
    # offerline_ids.each do |offerline_id|
    #   other_buyer_id = @redis.hget("offerline:#{offerline_id}:meta", 'buyer_id')
    #   next if other_buyer_id == buyer_id
    #   user_ids.add(other_buyer_id)
    # end

    # 群发喜欢该商品的用户商品已售出
    # liker_ids = @redis.smembers("item:#{item_id}:likers")
    liker_ids = @redis.zrange("item:#{item_id}:new_likers", 0, -1)
    liker_ids.each do |liker_id|
      # next if liker_id == buyer_id
      next if is_blocked?(owner, liker_id)
      user_ids.add(liker_id)
    end

    user_ids.to_a.each do |user_id|
      nickname = @redis.hget("user:#{user_id}", 'nickname')
      vars[:nickname] = nickname
      send_message(user_id, task, vars, false, owner)
    end
  end
end

# 给已offer过的买家
class BuyersMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    vars = options['data']
    item_id = vars['item_id']
    item_info = @redis.hmget("item:#{item_id}", 'id', 'title', 'images', 'local_price', 'state')
    owner = @redis.get("item:#{item_id}:owner")

    return if item_info[4].to_i != 0 # 0: item listing

    vars[:fuzzy_item_id] = item_info[0]
    vars[:item_title] = item_info[1]
    vars[:item_image] = JSON.load(item_info[2])[0]['imageLink']
    vars[:item_price] = item_info[3]

    offerline_ids = @redis.lrange("item:#{item_id}:offerlines", 0, -1)
    offerline_ids.each do |offerline_id|
      buyer_id = @redis.hget("offerline:#{offerline_id}:meta", 'buyer_id')
      nickname = @redis.hget("user:#{buyer_id}", 'nickname')
      vars[:nickname] = nickname
      send_message(buyer_id, task, vars, false, owner)
    end
  end
end

# 上传后24小时后Listing状态且未Renew过
class RenewMessageWorker < BaseMessageWorker
  def perform(task_id, options = {})
    task = Task.find(task_id)
    return if task.status == 0

    target_user = options['target_user']
    vars = options['data']
    item_id = vars['item_id']

    # p "item_id: #{item_id}, sold_to: #{@redis.get("item:#{item_id}:sold_to")}"
    item_info = @redis.hmget("item:#{item_id}", 'state', 'created_at', 'updated_at')
    if item_info[0].to_i != 0 or item_info[1] != item_info[2]
      # 状态不是listing或者已经renew过，什么都不做
      return
    end

    count_key = "fmmc:renew:user:#{target_user}:daily_count"
    daily_count = @redis.get(count_key)
    if daily_count && daily_count.to_i >= 1
      logger.info "Renew alert for user #{target_user} limit to one time per day"
      return
    end

    send_message(target_user, task, vars)

    @redis.pipelined do
      @redis.incr(count_key)
      @redis.expire(count_key, 24 * 3600)
    end
  end
end
