class RealSmsWorker

  include Sidekiq::Worker
  # sidekiq_options :backtrace => true
  # sidekiq_options :queue => :critical
  sidekiq_options :retry => false if Rails.env.development?
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  # 每个用户每天只能收到最多10条短信
  DAILY_QUOTA = 10

  def initialize
    @redis = Redis.new(:host => "#{Settings.biz_redis_host}", :port => Settings.biz_redis_port, :db => Settings.biz_redis_db)
  end

  def perform(target_user, mobile_phone, text)
    key = "fmmc:mobile_phone:#{mobile_phone}:daily_count"
    daily_count = @redis.get(key)
    if !daily_count
      @redis.incr(key)
      @redis.expire(key, 24 * 3600)
    elsif daily_count.to_i >= DAILY_QUOTA
      logger.info "mobile_phone:#{mobile_phone} exceed SMS daily quota of #{DAILY_QUOTA}"
      return
    else
      @redis.incr(key)
    end

    # text = text.truncate(160) if text.length() > 160

    begin
      from_numbers = Settings.twilio.from_numbers
      from_number = from_numbers[rand(from_numbers.length())]
      client = Twilio::REST::Client.new Settings.twilio.account_sid, Settings.twilio.auth_token
      message = client.account.messages.create(
          :from => from_number,
          :to =>   mobile_phone,
          :body => text
      )

      logger.info "[SMS] message_sid => #{message.sid}, target_user => #{target_user}, from => #{from_number}, to => #{mobile_phone}, text => #{text}"
    rescue Twilio::REST::RequestError => e
      logger.error "#{e.message}: target_user => #{target_user}, from => #{from_number}, to => #{mobile_phone}, text => #{text}"
      Raven.capture_exception(e, :target_user => target_user, :from => from_number, :to => mobile_phone, :text => text)
    end
  end
end