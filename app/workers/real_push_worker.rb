class RealPushWorker

  include Sidekiq::Worker
  # sidekiq_options :backtrace => true
  # sidekiq_options :queue => :critical
  sidekiq_options :retry => false if Rails.env.development?
  sidekiq_retries_exhausted do |msg|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
  end

  PUSH_PROVIDER_GETUI = 'getui'
  PUSH_PROVIDER_PARSE = 'parse'

  def initialize
    @redis = Redis.new(:host => "#{Settings.biz_redis_host}", :port => Settings.biz_redis_port, :db => Settings.biz_redis_db)
    @gt_push = GtPush.instance

    # v2.4开始使用Parse Push
    Parse.init :application_id => Settings.parse.app_id,
               :api_key        => Settings.parse.api_key,
               :master_key     => Settings.parse.master_key,
               :quiet          => true

    @provider = Settings.push.provider
  end

  def perform(target_user, text, payload = nil, title = nil, flag = nil, template_id = nil, daily_quota = -1, group = false, task_id = -1)
    if template_id.present? and daily_quota != -1
      key = "fmmc:push:#{target_user}:template:#{template_id}:daily_count"
      daily_count = @redis.get(key)
      if !daily_count
        @redis.incr(key)
        @redis.expire(key, 24 * 3600)
      elsif daily_count.to_i >= daily_quota
        logger.info "target user:#{target_user} exceed push daily quota of #{daily_quota}"
        return
      else
        @redis.incr(key)
      end
    end

    return do_push(target_user, text, payload, title, flag, group, task_id)
  end

  def do_push(target_user, text, payload = nil, title = nil, flag = nil, group = false, task_id = -1)
    installations = DeviceInstallation.where(user_id: target_user, provider: @provider)
    sent = Set.new
    installations.each do |installation|
      next if sent.include?(installation.installation_id)
      os = installation.os
      app_version = installation.app_version

      if payload.is_a?(String)
        payload_dict = JSON.parse(payload)
      else
        payload_dict = payload.clone
      end

      compatible_old_version(app_version, os, payload_dict, text, title, flag)

      if flag == 2
        sound = 'mute.wav'
      elsif app_version.present? && app_version >= '2.2'
        sound = '5miles.wav'
      end

      if @provider == PUSH_PROVIDER_GETUI
        data = payload_dict.to_json
        begin
          result = @gt_push.push_to_single(data, installation.installation_id, '1', text, sound, os)
        rescue => e
          result = {'result' => e.message}
        end
      elsif @provider == PUSH_PROVIDER_PARSE
        data = { :payload => payload_dict.to_json, :alert => { :body => text }, :badge => 1, :sound => sound }
        json = JSON.generate data
        if json.length > 256
          logger.warn "Push payload length over limit: #{json.length}. Allowed: 256, cut it automatically."
          text = text.truncate(text.length - (json.length - 256))
          data[:alert][:body] = text
        end
        result = push_via_parse(data, installation.installation_id, os)
      end
      logger.info "[PUSH] group => false, task_id => #{task_id}, provider => #{@provider}, installation_id => #{installation.installation_id}, result => #{result}, payload => #{data}, text => #{text}, target_user => #{target_user}, OS => #{installation.os}"
      sent << installation.installation_id
    end
  end

  def push_via_parse(data, installation_id, os)
    push = Parse::Push.new(data)
    query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq('installationId', installation_id).eq('deviceType', os)
    push.where = query.where
    push.save
  end

  def compatible_old_version(app_version, os, payload_dict, text, title, flag)
    # logger.info "after: payload: #{payload}, type: #{payload.class}"
    if app_version.blank?
      # 兼容V2.0
      if payload_dict['t'] == 's'
        payload_dict['type'] = 'system'
        if payload_dict['a'] == 'm'
          payload_dict['action'] = 'message'
        else
          payload_dict['action'] = 'home'
        end
        payload_dict.delete('a')
      elsif payload_dict['t'] == 'c'
        payload_dict['type'] = 'comment'
        payload_dict['item_id'] = payload_dict['id']
        payload_dict.delete('id')
      elsif payload_dict['t'] == 'o'
        payload_dict['type'] = 'offer'
        payload_dict['offerline_id'] = payload_dict['olid']
        payload_dict['msg_id'] = payload_dict['mid']
        payload_dict.delete('olid')
        payload_dict.delete('mid')
      end
      payload_dict.delete('t')
    end

    if os == Client::OS_TYPE_ANDROID
      if app_version.blank?
        payload_dict['text'] = text
      else
        payload_dict['txt'] = text
        payload_dict['title'] = title if title
        payload_dict['flag'] = flag if flag
      end
    elsif os == Client::OS_TYPE_IOS
      payload_dict.delete('img')  # iOS不支持自定义push notification图片，为避免payload超长，删除img字段
    end
  end
end
