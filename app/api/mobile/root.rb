require 'grape-swagger'

module Mobile
  class Root < Grape::API
    default_format :json

    mount Mobile::API => '/v1'
    mount Mobile::APIV2 => '/v2'
    add_swagger_documentation(
        base_path: "/api",
        hide_documentation_path: true,
        # api_version: 'v1',
        hide_format: true,
    )
  end
end