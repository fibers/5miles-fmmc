require 'will_paginate/array'
require 'api_errors'

module Mobile

  class API < Grape::API
    # version 'v1', using: :path, vendor: '5miles'
    format :json

    rescue_from APIErrors::AuthError do |e|
      Rack::Response.new({
                            request: env['api.endpoint'].request.path,
                            error_code: 9000,
                            error_msg: e.message
                        }.to_json, 401).finish
    end

    rescue_from ActiveRecord::RecordNotFound do
      Rack::Response.new({
                             request: env['api.endpoint'].request.path,
                             error_code: 9001,
                             error_msg: 'Not Found'
                         }.to_json, 404).finish
    end

    rescue_from :all do |e|
      Rack::Response.new({
                             request: env['api.endpoint'].request.path,
                             error_code: 9999,
                             error_msg: e.message
                         }.to_json, 500).finish
    end

    helpers do
      def logger
        API.logger
      end

      def authenticate!
        logger.debug "Authenticating: user_id => #{headers['X-Fivemiles-User-Id']}, token => #{headers['X-Fivemiles-User-Token']}"
        begin
          result = JWT.decode(headers['X-Fivemiles-User-Token'], Settings.jwt_secret)
          if result[0]['user_id'] != headers['X-Fivemiles-User-Id'].to_i
            raise APIErrors::AuthError, 'Unauthorized'
          end
        rescue
          raise APIErrors::AuthError, 'Unauthorized'
        end
      end

      def meta(collection)
        meta = Mobile::Meta.new
        meta.limit = collection.per_page
        meta.offset = collection.offset
        meta.total_count = collection.total_entries
        if collection.previous_page
          meta.previous = "?limit=#{meta.limit}&offset=#{meta.offset - meta.limit}"
        else
          meta.previous = nil
        end
        if collection.next_page
          meta.next = "?limit=#{meta.limit}&offset=#{meta.offset + meta.limit}"
        else
          meta.next = nil
        end
        meta
      end

      def find_offers_by_user_and_item(objects, user_id, user_type, item_id)
        offers = Offer.find_recent_by_user_and_item(user_id, user_type, item_id)
        if offers.size > 0
          offers.each do |offer|
            objects.unshift({  :type => 'OFFER',
                            :id => offer.id,
                            :sell_side => offer.sell_side,
                            :price => offer.price,
                            :comment => offer.comment,
                            :head => offer.sell_side == Constants::SellSide::BUYER_TO_SELLER ? offer.buyer_head : offer.seller_head,
                            :thread_id => offer.thread_id,
                            :status => user_type == 'seller' ? offer.seller_status : offer.buyer_status,
                            :accepted => offer.accepted?,
                            :timestamp => offer.created_at.to_i
                          })
          end
        end
        Offer.unread_num(user_id, user_type, item_id)
      end

      def find_chats_by_user_and_item(objects, user_id, user_type, item_id)
        chats = Chat.find_recent_by_user_and_item(user_id, user_type, item_id)
        if chats.size > 0
          chats.each do |chat|
            objects.unshift({  :type => 'CHAT',
                            :id => chat.id,
                            :content => chat.content,
                            :head => chat.sell_side == Constants::SellSide::BUYER_TO_SELLER ? chat.buyer_head : chat.seller_head,
                            :thread_id => chat.thread_id,
                            :status => user_type == 'seller' ? chat.seller_status : chat.buyer_status,
                            :timestamp => chat.created_at.to_i
                          })
          end
        end
        Chat.unread_num(user_id, user_type, item_id)
      end

      def find_feedbacks_by_user_and_item(objects, user_id, user_type, item_id)
        feedbacks = Feedback.find_recent_by_user_and_item(user_id, user_type, item_id)
        if feedbacks.size > 0
          feedbacks.each do |feedback|
            objects.unshift({  :type => 'FEEDBACK',
                            :id => feedback.id,
                            :rate_num => feedback.rate_num,
                            :comment => feedback.comment,
                            :head => feedback.sell_side == Constants::SellSide::BUYER_TO_SELLER ? feedback.buyer_head : feedback.seller_head,
                            :thread_id => feedback.thread_id,
                            :status => user_type == 'seller' ? feedback.seller_status : feedback.buyer_status,
                            :timestamp => feedback.created_at.to_i
                          })
          end
        end
        Feedback.unread_num(user_id, user_type, item_id)
      end

      def get_messages(user_id, user_type)
        question_items = Question.find_items_by_user(user_id, user_type)
        result = []
        item_ids = []
        if question_items.size > 0
          question_items.each do |question_item|
            objects = []
            item_id = question_item.item_id
            item_ids << item_id
            new_offers_count = find_offers_by_user_and_item(objects, user_id, user_type, item_id)
            new_chats_count = find_chats_by_user_and_item(objects, user_id, user_type, item_id)
            new_feedbacks_count = find_feedbacks_by_user_and_item(objects, user_id, user_type, item_id)

            result.push({:count => new_offers_count + new_chats_count + new_feedbacks_count,
                         :last_message => question_item.content,
                         :nick => question_item.is_reply? ? question_item.seller_nick : question_item.buyer_nick,
                         :item_title => question_item.item_title.truncate(20),
                         :item_id => item_id,
                         :item_image => question_item.item_image,
                         :objects => objects.size > 0 ? [objects[0]] : [],
                         :timestamp => question_item.created_at.to_i
                        })
          end
        end

        offer_items = Offer.find_items_by_user(user_id, user_type, item_ids)
        if offer_items.size > 0
          offer_items.each do |offer|
            objects = []
            item_id = offer.item_id
            new_offers_count = find_offers_by_user_and_item(objects, user_id, user_type, item_id)
            new_chats_count = find_chats_by_user_and_item(objects, user_id, user_type, item_id)
            new_feedbacks_count = find_feedbacks_by_user_and_item(objects, user_id, user_type, item_id)
            result.push({:count => new_offers_count + new_chats_count + new_feedbacks_count,
                         :item_title => offer.item_title.truncate(20),
                         :item_id => item_id,
                         :item_image => offer.item_image,
                         :objects => objects.size > 0 ? [objects[0]] : [],
                         :timestamp => offer.created_at.to_i
                        })
          end
        end

        result.sort_by! { |item| item[:timestamp] }.reverse!
        result.map { |entry| entry.delete(:timestamp) }

        params[:limit] ||= Settings.limit
        params[:offset] ||= 0
        limit = params[:limit].to_i
        offset = params[:offset].to_i
        page = (offset / limit) + 1
        result.paginate(:page => page, :per_page => limit)
      end
    end

    resource :messages do
      desc '获取购买中的商品消息列表'
      get :buying do
        authenticate!

        user_id = headers['X-Fivemiles-User-Id'].to_i
        messages = get_messages(user_id, Constants::UserType::BUYER)

        present :meta, meta(messages)
        present :objects, messages
      end

      desc '获取出售中的商品消息列表'
      get :selling do
        authenticate!

        user_id = headers['X-Fivemiles-User-Id'].to_i
        messages = get_messages(user_id, Constants::UserType::SELLER)

        present :meta, meta(messages)
        present :objects, messages
      end

      resource :system do
        desc '获取系统消息列表'
        get do
          authenticate!

          user_id = headers['X-Fivemiles-User-Id'].to_i
          params[:limit] ||= Settings.limit
          params[:offset] ||= 0
          limit = params[:limit].to_i
          offset = params[:offset].to_i
          page = (offset / limit) + 1
          messages = Message.where("user_id = ? and status != #{Constants::Status::DELETED}", user_id)
          .paginate(:page => page, :per_page => limit)
          .order('id DESC')

          present :meta, meta(messages)
          present :objects, messages, with: Mobile::Entities::Message
        end

        desc '标记所有系统消息为已读'
        put :read do
          authenticate!

          user_id = headers['X-Fivemiles-User-Id'].to_i
          messages = Message.where("user_id = ? and status != #{Constants::Status::DELETED}", user_id)
          messages.each do | message |
            message.mark_read
          end
        end

        desc '移除系统消息（标记为已删除）'
        delete ':id' do
          authenticate!
          m = Message.find(params[:id])
          m.mark_deleted
        end
      end

      desc '获取未读消息数'
      get :unread_num do
        authenticate!

        user_id = headers['X-Fivemiles-User-Id'].to_i
        unread_num = Message.unread_num(user_id) +
            Offer.unread_num(user_id, Constants::UserType::BUYER) +
            Offer.unread_num(user_id, Constants::UserType::SELLER) +
            Chat.unread_num(user_id, Constants::UserType::BUYER) +
            Chat.unread_num(user_id, Constants::UserType::SELLER) +
            Feedback.unread_num(user_id, Constants::UserType::BUYER) +
            Feedback.unread_num(user_id, Constants::UserType::SELLER)
        { unread_num: unread_num }
      end
    end

    resource :questions do
      resource ':item_id' do
        desc '标记Question或Reply为已读'
        put :read do
          authenticate!

          user_id = headers['X-Fivemiles-User-Id'].to_i
          # logger.debug "Mark question as read, item_id => #{params[:item_id]}, user_id => #{user_id}"
          questions = Question.where(
              'item_id = :item_id and ((buyer_id = :user_id and buyer_status != :status) or (seller_id = :user_id and seller_status != :status))',
              item_id: params[:item_id].to_i,
              user_id: user_id,
              status: Constants::Status::DELETED
          )
          questions.each do | q |
            q.mark_read(user_id)
          end
        end

=begin
        desc '标记Question或Reply为已读'
        put :read do
          authenticate!
          q = Question.find(params[:id])
          q.mark_read(headers['X-Fivemiles-User-Id'].to_i)
        end

        desc '移除Question或者Reply消息（标记为已删除）'
        delete do
          authenticate!
          q = Question.find(params[:id])
          q.mark_deleted(headers['X-Fivemiles-User-Id'].to_i)
        end
=end
      end
    end

    resource :offers do
      desc '移除Offer消息（标记为已删除）'
      delete ':id' do
        authenticate!
        o = Offer.find(params[:id])
        o.mark_deleted(headers['X-Fivemiles-User-Id'].to_i)
      end
    end

    resource :chats do
      desc '移除Chat消息（标记为已删除）'
      delete ':id' do
        authenticate!
        c = Chat.find(params[:id])
        c.mark_deleted(headers['X-Fivemiles-User-Id'].to_i)
      end
    end

    resource :feedbacks do
      resource ':id' do
        desc '移除Feedback消息（标记为已删除，以后可能会去掉该接口！）'
        delete do
          authenticate!
          f = Feedback.find(params[:id])
          f.mark_deleted(headers['X-Fivemiles-User-Id'].to_i)
        end

        desc '标记Feedback为已读'
        put :read do
          authenticate!
          f = Feedback.find(params[:id])
          f.mark_read(headers['X-Fivemiles-User-Id'].to_i)
        end
      end
    end

    resource :threads do
      resource ':thread_id' do
        desc '标记一个thread为已读'
        put :read do
          authenticate!

          user_id = headers['X-Fivemiles-User-Id'].to_i
          sql = 'thread_id = :thread_id and (buyer_id = :user_id or seller_id = :user_id)'
          thread_id = params[:thread_id].to_i
          offers = Offer.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          offers.each do | offer |
            offer.mark_read(user_id)
          end

          chats = Chat.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          chats.each do | chat |
            chat.mark_read(user_id)
          end

          feedbacks = Feedback.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          feedbacks.each do | feedback |
            feedback.mark_read(user_id)
          end
        end
      end
    end

    resource :clients do
      desc '绑定用户ID与个推ClientID'

      params do
        requires :client_id, type: String, desc: '个推Client ID'
        requires :os, type: String, desc: '操作系统版本：os / android'
        requires :os_version, type: String, desc: '操作系统版本号'
        optional :mobile_phone, type: String, desc: '手机号，必须带国际区号，比如+8618600000000'
      end

      post :bind do
        authenticate!
        clients =  Client.where(
            'user_id = :user_id and client_id = :client_id',
            user_id: headers['X-Fivemiles-User-Id'],
            client_id: params[:client_id]
        )
        if clients.size == 0
          clients =  Client.where(
              'client_id = :client_id',
              client_id: params[:client_id]
          )
          clients.each do |c|
            c.update_attributes( status: Client::Status::UNBIND)
          end

          Client.create!(
              user_id: headers['X-Fivemiles-User-Id'],
              client_id: params[:client_id],
              os: params[:os].downcase,
              os_version: params[:os_version],
              mobile_phone: params[:mobile_phone],
              status: Client::Status::BIND
          )
        else
          clients[0].update_attributes(
              client_id: params[:client_id],
              os: params[:os].downcase,
              os_version: params[:os_version],
              mobile_phone: params[:mobile_phone],
              status: Client::Status::BIND
          )
          if clients[0].status == Client::Status::UNBIND
            # TODO 用户从UNBIND状态变为BIND状态，需检查是否有发送给该用户的消息

          end
        end
      end

      desc '取消绑定用户ID与个推ClientID'

      params do
        requires :client_id, type: String, desc: '个推Client ID'
      end

      delete :unbind do
        authenticate!
        clients =  Client.where(
            'user_id = :user_id and client_id = :client_id',
            user_id: headers['X-Fivemiles-User-Id'],
            client_id: params[:client_id]
        )
        if clients.size == 0
          raise ActiveRecord::RecordNotFound, "User##{headers['X-Fivemiles-User-Id']} not found"
        else
          clients[0].update_attributes( status: Client::Status::UNBIND)
        end
      end
    end

    resource :sms_dlr_callback do
      desc 'Nexmo短信状态报告回调（未实现，以后可能删除）'
      # Request example:
      # ?msisdn=66837000111&to=12150000025
      # &network-code=52099&messageId=000000FFFB0356D2
      # &price=0.02000000&status=delivered
      # &scts=1208121359&err-code=0&message-timestamp=2012-08-12+13%3A59%3A37

      #params do
      #  requires :msisdn, type: String
      #end

      get do
        logger.debug "[Not implement yet!!!]Receiving SMS Delivery Receipt callback: #{env['api.endpoint'].request.url}"
        # TODO 实现短信状态报告回调处理逻辑
      end

    end

    resource :push_callback do
      desc '个推push结果回调（未实现）'

      get do
        logger.debug "[Not implement yet!!!]Receiving push callback: #{env['api.endpoint'].request.url}"
        # TODO 实现push回调处理逻辑
      end
    end

    # route :any, '*path' do
    #   raise APIErrors::CommonError, 'Invalid Request URL'
    # end
  end

  class Meta
    attr_accessor :limit, :offset, :total_count, :previous, :next
  end
end