require 'will_paginate/array'
require 'api_errors'

module Mobile

  class APIV2 < Grape::API
    # version 'v2', using: :path, vendor: '5miles'
    format :json

    rescue_from APIErrors::AuthError do |e|
      Rack::Response.new({
                             request: env['api.endpoint'].request.path,
                             error_code: 9000,
                             error_msg: e.message
                         }.to_json, 401).finish
    end

    rescue_from ActiveRecord::RecordNotFound do
      Rack::Response.new({
                             request: env['api.endpoint'].request.path,
                             error_code: 9001,
                             error_msg: 'Not Found'
                         }.to_json, 404).finish
    end

    rescue_from :all do |e|
      Rack::Response.new({
                             request: env['api.endpoint'].request.path,
                             error_code: 9999,
                             error_msg: e.message
                         }.to_json, 500).finish
    end

    helpers do
      def logger
        APIV2.logger
      end

      def authenticate!
        logger.debug "Authenticating: user_id => #{headers['X-Fivemiles-User-Id']}, token => #{headers['X-Fivemiles-User-Token']}"
        begin
          result = JWT.decode(headers['X-Fivemiles-User-Token'], Settings.jwt_secret)
          if result[0]['user_id'].blank?
            raise APIErrors::AuthError, 'Unauthorized'
          end
        rescue
          raise APIErrors::AuthError, 'Unauthorized'
        end
        result[0]['user_id']
      end

      def meta(collection)
        meta = Mobile::Meta.new
        meta.limit = collection.per_page
        meta.offset = collection.offset
        meta.total_count = collection.total_entries
        if collection.previous_page
          meta.previous = "?limit=#{meta.limit}&offset=#{meta.offset - meta.limit}"
        else
          meta.previous = nil
        end
        if collection.next_page
          meta.next = "?limit=#{meta.limit}&offset=#{meta.offset + meta.limit}"
        else
          meta.next = nil
        end
        meta
      end

      def find_offers_by_user_and_item(objects, user_id, user_type, item_id)
        offer = Offer.find_recent_by_user_and_item(user_id, user_type, item_id, Offer::OFFER)
        chat = Offer.find_recent_by_user_and_item(user_id, user_type, item_id, Offer::CHAT)
        if !offer.empty? && chat.empty?
          objects.unshift(offer)
        elsif offer.empty? && !chat.empty?
          chat.delete(:price)
          chat.delete(:accepted)
          objects.unshift(chat)
        elsif !offer.empty? && !chat.empty?
          chat.delete(:price)
          if chat[:timestamp] > offer[:timestamp]
            offer[:timestamp] = chat[:timestamp]
            offer[:message] = chat[:message]
          end
          offer.delete(:offer_type)
          objects.unshift(offer)
        end

        Offer.unread_num(user_id, user_type, item_id)
      end

      def find_feedbacks_by_user_and_item(objects, user_id, user_type, item_id)
        feedback = Feedback.find_recent_by_user_and_item(user_id, user_type, item_id)
        if !feedback.empty?
          chat = Offer.find_recent_by_user_and_item(user_id, user_type, item_id, Offer::CHAT)
          if chat.empty?
            objects.unshift(feedback)
          else
            if chat[:timestamp] > feedback[:timestamp]
              feedback[:timestamp] = chat[:timestamp]
              feedback[:message] = chat[:message]
            end
            objects.unshift(feedback)
          end
        end
        Feedback.unread_num(user_id, user_type, item_id)
      end

      def get_messages(user_id, user_type)
        entries = []
        items = Offer.find_items_by_user(user_id, user_type)
        if items.size > 0
          items.each do |item|
            objects = []
            item_id = item.item_id
            new_offers_count = find_offers_by_user_and_item(objects, user_id, user_type, item_id)
            new_feedbacks_count = find_feedbacks_by_user_and_item(objects, user_id, user_type, item_id)
            entry = {
                :unread_num => new_offers_count + new_feedbacks_count,
                :item_image => item.item_image,
                :head => user_type == Constants::UserType::BUYER ? item.seller_head : item.buyer_head,
                :nick => user_type == Constants::UserType::BUYER ? item.seller_nick : item.buyer_nick,
            }
            if objects.size > 0
              entry.merge!(objects[0])
            end
            entries.push(entry)
          end
        end

        entries.sort_by! { |item| item[:timestamp] }.reverse!
        # entries.map { |entry| entry.delete(:timestamp) }

        params[:limit] ||= Settings.limit
        params[:offset] ||= 0
        limit = params[:limit].to_i
        offset = params[:offset].to_i
        page = (offset / limit) + 1
        entries.paginate(:page => page, :per_page => limit)
      end
    end

    resource :messages do
      desc '获取购买中的商品消息列表'
      get :buying do
        user_id = authenticate!
        messages = get_messages(user_id, Constants::UserType::BUYER)

        present :meta, meta(messages)
        present :objects, messages
      end

      desc '获取出售中的商品消息列表'
      get :selling do
        user_id = authenticate!
        messages = get_messages(user_id, Constants::UserType::SELLER)

        present :meta, meta(messages)
        present :objects, messages
      end

      resource :system do
        desc '获取系统消息列表'
        get do
          user_id = authenticate!
          params[:limit] ||= Settings.limit
          params[:offset] ||= 0
          limit = params[:limit].to_i
          offset = params[:offset].to_i
          page = (offset / limit) + 1
          messages = Message.where("user_id = ? and status != #{Constants::Status::DELETED}", user_id)
          .paginate(:page => page, :per_page => limit)
          .order('id DESC')

          present :meta, meta(messages)
          present :objects, messages, with: Mobile::Entities::Message
        end

        desc '标记所有系统消息为已读'
        put :read do
          user_id = authenticate!
          messages = Message.where("user_id = ? and status != #{Constants::Status::DELETED}", user_id)
          messages.each do | message |
            message.mark_read
          end

          body nil
        end

        desc '移除系统消息（标记为已删除）'
        delete ':id' do
          user_id = authenticate!
          m = Message.find(params[:id])
          m.mark_deleted
        end
      end

      desc '获取未读消息数'
      get :unread_num do
        user_id = authenticate!
        unread_num = Message.unread_num(user_id) +
            Offer.unread_num(user_id, Constants::UserType::BUYER) +
            Offer.unread_num(user_id, Constants::UserType::SELLER) +
            Feedback.unread_num(user_id, Constants::UserType::BUYER) +
            Feedback.unread_num(user_id, Constants::UserType::SELLER)
        { unread_num: unread_num }
      end
    end

    resource :threads do
      resource ':thread_id' do
        desc '标记一个thread为已读'
        put :read do
          user_id = authenticate!
          sql = 'thread_id = :thread_id and (buyer_id = :user_id or seller_id = :user_id)'
          thread_id = params[:thread_id].to_i
          offers = Offer.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          offers.each do | offer |
            offer.mark_read(user_id)
          end

          feedbacks = Feedback.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          feedbacks.each do | feedback |
            feedback.mark_read(user_id)
          end

          body nil
        end

        desc '移除一个thread中的所有消息（标记为已删除）'
        delete do
          user_id = authenticate!
          sql = 'thread_id = :thread_id and (buyer_id = :user_id or seller_id = :user_id)'
          thread_id = params[:thread_id].to_i
          offers = Offer.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          offers.each do | offer |
            offer.mark_deleted(user_id)
          end

          feedbacks = Feedback.where(
              sql,
              thread_id: thread_id,
              user_id: user_id
          )
          feedbacks.each do | feedback |
            feedback.mark_deleted(user_id)
          end

          body nil
        end
      end
    end

    resource :clients do
      desc '绑定用户ID与个推ClientID'

      params do
        requires :client_id, type: String, desc: '个推Client ID'
        requires :os, type: String, desc: '操作系统版本：os / android'
        requires :os_version, type: String, desc: '操作系统版本号'
        optional :mobile_phone, type: String, desc: '手机号，必须带国际区号，比如8618600000000'
      end

      post :bind do
        user_id = authenticate!
        clients =  Client.where(
            'user_id = :user_id and client_id = :client_id',
            user_id: user_id,
            client_id: params[:client_id]
        )
        if clients.size == 0
          clients =  Client.where(
              'client_id = :client_id',
              client_id: params[:client_id]
          )
          clients.each do |c|
            c.update_attributes( status: Client::Status::UNBIND)
          end

          Client.create!(
              user_id: user_id,
              client_id: params[:client_id],
              os: params[:os].downcase,
              os_version: params[:os_version],
              mobile_phone: params[:mobile_phone],
              status: Client::Status::BIND
          )
        else
          clients[0].update_attributes(
              client_id: params[:client_id],
              os: params[:os].downcase,
              os_version: params[:os_version],
              mobile_phone: params[:mobile_phone],
              status: Client::Status::BIND
          )
          if clients[0].status == Client::Status::UNBIND
            # TODO 用户从UNBIND状态变为BIND状态，需检查是否有发送给该用户的消息

          end
        end

        body nil
      end

      desc '取消绑定用户ID与个推ClientID'

      params do
        requires :client_id, type: String, desc: '个推Client ID'
      end

      delete :unbind do
        user_id = authenticate!
        clients =  Client.where(
            'user_id = :user_id and client_id = :client_id',
            user_id: user_id,
            client_id: params[:client_id]
        )
        if clients.size == 0
          raise ActiveRecord::RecordNotFound, "User##{user_id} not found"
        else
          clients[0].update_attributes( status: Client::Status::UNBIND)
        end
      end
    end

    # route :any, '*path' do
    #   raise APIErrors::CommonError, 'Invalid Request URL'
    # end
  end

  class Meta
    attr_accessor :limit, :offset, :total_count, :previous, :next
  end
end