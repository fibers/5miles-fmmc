module Mobile
  module Entities
    class Message < Grape::Entity
      expose :id
      # expose :content do |message, options|
      #   "#{message.content.truncate(28)}"
      # end
      expose :content
      format_with(:timestamp) { |dt| dt.to_i }
      with_options(format_with: :timestamp) do
        expose :created_at, as: :timestamp
      end
      expose :status
    end
  end
end