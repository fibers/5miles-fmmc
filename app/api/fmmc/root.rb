require 'grape-swagger'

module Fmmc
  class Root < Grape::API
    default_format :json

    mount Fmmc::API => '/'
    add_swagger_documentation(
        base_path: "/api",
        hide_documentation_path: true,
        # api_version: 'v1',
        hide_format: true,
    )
  end
end