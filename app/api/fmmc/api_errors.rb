module APIErrors
  class AuthError < StandardError; end
  class CommonError < StandardError; end
end