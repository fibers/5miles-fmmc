require 'message_worker'


module Fmmc
  class API < Grape::API
    version 'v2.1', using: :header, vendor: 'fmmc'
    format :json

    # Grape::Middleware::Auth::Strategies.add(:fmmc_auth, AuthMiddelware, ->(options){[options[:realm]]})
    #
    # auth :fmmc_auth, {realm: 'Fmmc API'} do ||

    helpers do
      def logger
        API.logger
      end
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      API.logger.error e
      # Raven.capture_exception(e)
      Rack::Response.new({
                             message: e.message
                         }.to_json, 422).finish
    end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      API.logger.error e
      # Raven.capture_exception(e)
      Rack::Response.new({
                             message: e.message,
                         }.to_json, e.status)
    end

    rescue_from :all do |e|
      API.logger.error e
      # Raven.capture_exception(e)
      Rack::Response.new({
                             message: e.message,
                         }.to_json, 422)
    end

    resource :push do
      desc '推送消息'
      params do
        requires :target_user, type: String, desc: '目标用户ID'
        optional :source_user, type: String, desc: '来源用户ID'
        optional :template, type: String, desc: 'push模板名，模板携带了text, title, payload'
        optional :title, type: String, desc: '通知标题，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :text, type: String, desc: '通知文本，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :data, type: String, desc: '填充模板的数据'
        optional :payload, type: String, desc: 'push payload，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :flag, type: Integer, desc: '通知提醒方式'
        mutually_exclusive :template, :text
        exactly_one_of :template, :text
      end

      post do
        if !params[:template].blank?
          push_template = PushTemplate.find_by_name(params[:template])
          if not push_template
            raise ActiveRecord::RecordNotFound, "Template #{params[:template]} not found"
          elsif push_template.status == 0
            raise APIErrors::CommonError, "Template #{params[:template]} disabled"
          end
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        if not params[:payload].blank?
          payload = JSON.load(params[:payload])
        end

        PushWorker.perform_async(params[:target_user], params[:template], params[:text], data, payload, params[:title],
                                 params[:flag], params[:source_user])
      end
    end

    resource :send_mail do
      desc '发送邮件'
      params do
        requires :target_user, type: String, desc: '目标用户ID'
        optional :from, type: String, desc: '发送方邮件地址，如："Andy from 5miles" <Andy@5milesapp.com>，如使用模板则无需提供'
        optional :template, type: String, desc: '邮件模板名，模板携带了text, from, subject'
        optional :text, type: String, desc: '邮件文本，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :subject, type: String, desc: '邮件标题，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :data, type: String, desc: '填充模板的数据'
        mutually_exclusive :template, :text
        exactly_one_of :template, :text
      end

      post do
        if !params[:template].blank?
          mail_template = MailTemplate.find_by_name(params[:template])
          if not mail_template
            raise ActiveRecord::RecordNotFound, "Template #{params[:template]} not found"
          elsif mail_template.status == 0
            raise APIErrors::CommonError, "Template #{params[:template]} disabled"
          end
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        MailWorker.perform_async(params[:target_user], params[:template], params[:text], data, params[:subject],
                                 params[:from])
      end
    end


    resource :send_direct_mail do
      desc '发送邮件（使用邮件地址而不是User ID）'
      params do
        requires :to, type: String, desc: '接收方邮件地址'
        optional :from, type: String, desc: '发送方邮件地址，如："Andy from 5miles" <Andy@5milesapp.com>，如使用模板则无需提供'
        optional :template, type: String, desc: '邮件模板名，模板携带了text, from, subject'
        optional :text, type: String, desc: '邮件文本，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :subject, type: String, desc: '邮件标题，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :data, type: String, desc: '填充模板的数据'
        mutually_exclusive :template, :text
        exactly_one_of :template, :text
        # all_or_none_of :text, :subject 0.9.1才支持
      end

      post do
        if !params[:template].blank?
          mail_template = MailTemplate.find_by_name(params[:template])
          if not mail_template
            raise ActiveRecord::RecordNotFound, "Template #{params[:template]} not found"
          elsif mail_template.status == 0
            raise APIErrors::CommonError, "Template #{params[:template]} disabled"
          end
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        DirectMailWorker.perform_async(params[:to], params[:template], params[:text], data, params[:subject], params[:from])
      end
    end

    resource :notify do
      desc '推送或者发送邮件'
      params do
        optional :target_user, type: String, desc: '目标用户ID'
        optional :source_user, type: String, desc: '来源用户ID'
        requires :trigger_name, type: String, desc: '触发器名'
        optional :data, type: String, desc: '填充模板的数据'
      end

      post do
        trigger_name = params[:trigger_name]
        target_user = params[:target_user]
        data = params[:data]
        source_user = params[:source_user]
        trigger = Trigger.find_by_name(trigger_name)
        if not trigger
          raise ActiveRecord::RecordNotFound, "Trigger #{trigger_name} not found"
        end

        if trigger.status == Trigger::DISABLED
          raise APIErrors::CommonError, "Trigger #{trigger_name} disabled"
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        if trigger.trigger_type == Trigger::TRIGGER_TYPE_DELAYED
          if trigger.interval_quantity
            # 指定了间隔时间：frequency_quantity.frequency_period.from_now
            scheduled_at = trigger.interval.send(:from_now)
          else
            # 指定了时刻（默认为第二天某个时刻）
            time = trigger.at.split(':')
            if time.length() == 1
              time[1] = 0
            end

            tz = TZInfo::Timezone.get('America/Los_Angeles') # FIXME: 暂定为洛杉矶时区
            scheduled_at = tz.local_to_utc(tz.now.tomorrow.to_date + time[0].to_i.hours + time[1].to_i.minutes)
          end
        elsif trigger.trigger_type == Trigger::TRIGGER_TYPE_INSTANT
          # 立即执行
          scheduled_at = nil
        end

        task_cls = Object.const_get(trigger.task.clazz)
        if scheduled_at
          task_cls.send :perform_at, scheduled_at, trigger.task.id, {:target_user => target_user, :data => data,
                                                                     :source_user => source_user}
        else
          task_cls.send :perform_async, trigger.task.id, {:target_user => target_user, :data => data,
                                                          :source_user => source_user}
        end
      end
    end

    resource :system_message do
      desc '发送系统消息'
      params do
        requires :target_user, type: String, desc: '目标用户ID'
        optional :template, type: String, desc: '系统消息模板名'
        optional :data, type: String, desc: '填充模板的数据'
        optional :text, type: String, desc: '系统消息文本'
        optional :image, type: String, desc: '图片URL'
        optional :action, type: String, desc: 'm/p:#{user_id}/h/i:#{item_id}/u:#{url}/s:#{keyword}/c:#{category_id}/f:#{featured_items_id}/s2:#{keyword},cid:#{campain_id}/n/r'
        optional :desc, type: String, desc: '描述'
        optional :sm_type, type: Integer, values: [0, 1], default: 0, desc: 'notification类型，0: common, 1: review...'
        optional :image_type, type: Integer, values:[0, 1], desc: '图片类型：0: item image, 1: user portrait'
        optional :review_score, type: Integer, values: 1..5, desc: '评价分数'
        mutually_exclusive :template, :text
        exactly_one_of :template, :text
      end

      post do
        if !params[:template].blank?
          smt = SystemMessageTemplate.find_by_name(params[:template])
          if not smt
            raise ActiveRecord::RecordNotFound, "Template #{params[:template]} not found"
          elsif smt.status == 0
            raise APIErrors::CommonError, "Template #{params[:template]} disabled"
          end
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        options = {image: params[:image], action: params[:action], desc: params[:desc], sm_type: params[:sm_type],
                   image_type: params[:image_type], review_score: params[:review_score]}
        SystemMessageWorker.perform_async(params[:target_user], params[:template], params[:text], data, options)
      end
    end

    resource :send_sms do
      desc '发送短信'
      params do
        optional :target_user, type: String, desc: '目标用户ID'
        optional :mobile_phone, type: String, desc: '目标用户手机号码'
        optional :template, type: String, desc: 'SMS模板名'
        optional :text, type: String, desc: '通知文本，如使用模板则无需提供，如提供则会覆盖模板中相应字段'
        optional :data, type: String, desc: '填充模板的数据'
        mutually_exclusive :template, :text
        exactly_one_of :template, :text
        mutually_exclusive :target_user, :mobile_phone
        exactly_one_of :target_user, :mobile_phone
      end

      post do
        if params[:template].present?
          sms_template = SmsTemplate.find_by_name(params[:template])
          if not sms_template
            raise ActiveRecord::RecordNotFound, "Template #{params[:template]} not found"
          elsif sms_template.status == 0
            raise APIErrors::CommonError, "Template #{params[:template]} disabled"
          end
        end

        if not params[:data].blank?
          data = JSON.load(params[:data])
        end

        SmsWorker.perform_async(params[:target_user], params[:mobile_phone], params[:template], params[:text], data)
      end
    end
  end
end
