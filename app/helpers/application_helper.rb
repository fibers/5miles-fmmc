module ApplicationHelper

  # Returns the full title on a per-page basis.
  def full_title(page_title)
    base_title = "5miles Message Center"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def message_types
    Template::MESSAGE_TYPES
  end

  def client_status
    Client::Status
  end

  def message_status(msg)
    if msg.respond_to?(:is_reply)
      msg.is_reply? ? status = msg.buyer_status : status = msg.seller_status
    elsif msg.respond_to?(:sell_side)
      msg.sell_side == Constants::SellSide::SELLER_TO_BUYER ? status = msg.buyer_status : status = msg.seller_status
    else
      status = msg.status
    end
    Constants::Status::VALUE_MAP[status]
  end

  def push_status(status)
    case status
      when 0
        'Fresh'
      when 1
        'Success'
      when 2
        'Failure'
      else
        'Unknown'
    end
  end

  def head_image_tag(image, alt)
    cl_image_tag(image.sub('http://res.cloudinary.com/fivemiles/image/upload/', ''), :alt => alt, :size => '80x80', :crop => 'fill', :radius => 'max')
  end

  def item_image_tag(image, alt)
    cl_image_tag(image.sub('http://res.cloudinary.com/fivemiles/image/upload/', ''), :alt => alt, :size => '200x200', :crop => 'fit')
  end

  def bootstrap_class_for(flash_type)
    case flash_type
      when 'success'
        'alert-success'
      when 'error'
        'alert-danger'
      when 'alert'
        'alert-warning'
      when 'notice'
        'alert-info'
      else
        flash_type.to_s
    end
  end
end
