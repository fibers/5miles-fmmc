class QuestionsController < ApplicationController

  # GET /questions
  # GET /questions.json
  def index
    @pushes = initialize_grid(
        Push,
        # :include => [:pushable],
        :conditions => { pushable_type: 'Question'},
        :order => 'id',
        :order_direction => 'DESC'
    )

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pushes }
    end
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    @push = Push.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push }
    end
  end
end
