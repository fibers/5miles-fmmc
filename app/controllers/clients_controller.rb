class ClientsController < ApplicationController

  # GET /clients
  # GET /clients.json
  def index
    @clients = initialize_grid(Client,
                               :order => 'updated_at',
                               :order_direction => 'DESC',)
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @client = Client.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @client }
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    @client = Client.find(params[:id])
    @client.destroy

    respond_to do |format|
      format.html { redirect_to clients_url, notice: 'client was successfully removed.' }
      format.json { head :no_content }
    end
  end
end
