class FeedbacksController < ApplicationController

  # GET /feedbacks
  # GET /feedbacks.json
  def index
    @pushes = initialize_grid(
        Push,
        :conditions => { pushable_type: 'Feedback'},
        :order => 'id',
        :order_direction => 'DESC'
    )
  end

  # GET /feedbacks/1
  # GET /feedbacks/1.json
  def show
    @push = Push.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push }
    end
  end
end
