class MessagesController < ApplicationController

  # GET /messages
  # GET /messages.json
  def index
    @pushes = initialize_grid(
        Push,
        :conditions => { pushable_type: 'Message'},
        :order => 'id',
        :order_direction => 'DESC'
    )
  end

  # GET /messages/1
  # GET /messages/1.json
  def show
    @push = Push.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push }
    end
  end
end
