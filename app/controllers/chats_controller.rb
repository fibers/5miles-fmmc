class ChatsController < ApplicationController

  # GET /chats
  # GET /chats.json
  def index
    @pushes = initialize_grid(
        Push,
        :conditions => { pushable_type: 'Chat'},
        :order => 'id',
        :order_direction => 'DESC'
    )
  end

  # GET /chats/1
  # GET /chats/1.json
  def show
    @push = Push.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push }
    end
  end
end
