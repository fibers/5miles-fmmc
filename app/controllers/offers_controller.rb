class OffersController < ApplicationController

  # GET /offers
  # GET /offers.json
  def index
    @pushes = initialize_grid(
        Push,
        :conditions => { pushable_type: 'Offer'},
        :order => 'id',
        :order_direction => 'DESC'
    )
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
    @push = Push.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push }
    end
  end
end
